$('.telefone').mask('(99)9999-9999?9');
$(".dinheiro").maskMoney({symbol:'',showSymbol:true, thousands:'.', decimal:',', symbolStay: true});

// PESQUISA RAPIDA
function pesquisa_rapida(seletor, filtros)
{
    filtros = filtros.split(',');

    seletor.selectpicker('val', filtros);

    $('.form-filtro').submit();
}


function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


function obter_cidades_pela_uf(uf, simpleRequest)
{
    ajaxPost(
        uf,
        $('#base_url').val() + 'cidade/obter_cidade_pela_uf',
        simpleRequest
    );
}

$("#modal-dados").on('show.bs.modal', function () {
    console.log('awdawdwadaw');
    if($('#form-dados select[name=uf]').val() != undefined && $('#form-dados select[name=uf]').val() != '')
        obter_cidades($('#form-dados'), $('#form-dados select[name=cidade]').data('cidade-default'));
});