var maxResultado = 12;

var imovelVisualizacao = $('#imovel-visualizacao').clone();

$(document).ready(function(){
    if(window.location.pathname == '/imovel/pesquisar' || window.location.pathname == '/passo-fundo/imovel/pesquisar')
    {
        var pag = getParameterByName('pagina', location.href);

        pesquisar(pag, pag != null);
    }

    $('#ordenar_por').on('change', function(){
        pesquisar(0);
    });
});

function pesquisar(pagina, forcar_total)
{
    if(window.location.pathname != '/imovel/pesquisar' && window.location.pathname != '/passo-fundo/imovel/pesquisar')
    {
        $('#filtro-imovel').submit();
    }
    else
    {
        var msg_encontrados = '';
        if(pagina > 0)
            msg_encontrados = $('#msg-resultados-encontrados').html();

        $('#msg-resultados-encontrados').html('Buscando <strong>imóveis</strong>, aguarde ...');

        $('.imoveis-resultado-pesquisa').children().fadeOut(800, function(){
            $(this).remove();
        });

        filtro = {};

        filtro.pagina = pagina;

        if(forcar_total)
            filtro.forcar_total = 1;

        filtro.limite = maxResultado;
        filtro.ordenacao = $('#ordenar_por').val();

        //ATUALIZA URL SEM RECARREGAR A PÁGINA

        window.history.pushState('', document.title, $('#base_url').val() + 'imovel/pesquisar?' + $.param($('#filtro-imovel').jsonify()) + '&' + $.param(filtro));
        filtro.filtro = $('#filtro-imovel').jsonify();

        ajaxPost(
            filtro,
            base_url_filial('imovel/buscar', true),
            {
                successCallback: function (data) {
                    $.each(data.imoveis, function (key, imovel) {
                        console.log(imovel);
                        $('.imoveis-resultado-pesquisa').append(montar_imovel(imovel));
                    });

                    if (data.total != undefined && data.total > 0)
                    {
                        $('#msg-resultados-encontrados').html('<strong>Encontramos ' + data.total + '</strong> <br> <small>ofertas em nosso site.</small>');
                        atualizar_paginacao(parseInt(filtro.pagina) + 1, data.total);
                    }
                    else if(data.total != undefined)
                    {
                        $('#msg-resultados-encontrados').html('Nada foi encontrado.');
                        atualizar_paginacao(parseInt(filtro.pagina) + 1, 0);
                    }
                    else
                        $('#msg-resultados-encontrados').html(msg_encontrados);
                },
                failureCallback: function (data) {
                    alertify.error('Ocorreu um erro ao obter os imóveis.')
                },
                errorCallback: function (jqXhr, textStatus, errorThrown) {
                    alertify.error('Ocorreu um erro ao obter os imóveis. Tente novamente mais tarde.')
                },
                completeCallback: function () {
                }
            }
        );
    }
}

function montar_imovel(imovel)
{
    novaDiv = imovelVisualizacao.clone();

    var condominio_nome;
    var i = 0;

    for(i = 0; i < condominios.length; i++)
    {
        if(condominios[i].id == imovel.id_condominio)
        {
            condominio_nome = condominios[i].nome;
            break;
        }
    }

    novaDiv.find('.imovel-img')[0].style.backgroundImage = 'url(' + $('#filial_fotos_imoveis').val() + imovel.foto + '), url(' + base_url_filial('assets/images/imovel-sem-foto.jpg',$('#base_url').val())+') ';

    if(imovel.id_condominio > 0 && condominios[imovel.id_condominio])
    {
        novaDiv.find('.imovel-tipo').text(imoveis_tipos[imovel.id_tipo].tipo + ' em ' + condominio_nome);
    }
    else
    {
        novaDiv.find('.imovel-tipo').text(imoveis_tipos[imovel.id_tipo].tipo);
    }

    novaDiv.find('.imovel-url').attr('href', $('#base_url').val() + 'imovel?id=' + imovel.id);

    novaDiv.find('.imovel-cidade').html(imovel.cidade);
    novaDiv.find('.imovel-codigo').html(' Cód. ' + imovel.id);

    if(imovel.descricao.length > 185)
        imovel.descricao = imovel.descricao.substr(0, 182) + '...';

    novaDiv.find('.imovel-descricao').text(imovel.descricao);

    novaDiv.find('.imovel-valor').html(exibir_valor_imovel(imovel, 'R$'));

    novaDiv.show();

    return novaDiv;
}

function atualizar_paginacao(paginaAtual, items)
{
    $('#pagination').pagination({
        items: items,
        itemsOnPage: maxResultado,
        cssStyle: 'light-theme',
        nextText: '',
        prevText: '',
        currentPage: paginaAtual,
        onPageClick : function(pageNumber){
            pesquisar((pageNumber-1), maxResultado);
        }
    });
}