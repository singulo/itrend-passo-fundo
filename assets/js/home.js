var banner_condominios_currente_page = 0;

function banner_condominios_next_page()
{
    banner_condominios_currente_page ++;

    if(banner_condominios_currente_page > $('#banner-condominios ul li').length -1)
        banner_condominios_currente_page = 0;

    mostra_condominio_no_banner();
}

function banner_condominios_previous_page()
{
    banner_condominios_currente_page --;

    if(banner_condominios_currente_page < 0)
        banner_condominios_currente_page = $('#banner-condominios ul li').length -1;

    mostra_condominio_no_banner();
}

function mostra_condominio_no_banner()
{
    $('#banner-condominios ul li').hide();
    $($('#banner-condominios ul li')[banner_condominios_currente_page]).show();
}

//MOSTRA PRIMEIRO CONDOMINIO
mostra_condominio_no_banner();

$(document).ready(function(){
    setInterval(function()
    {
        banner_condominios_previous_page();
    }, 3000);
});