jQuery(document).ready(function($) {
    // VALOR SLIDER
    $('.slider-filtro').slider({
        formatter: function(value) {

            if(value[0] != undefined)
                atualiza_label_no_slider_valor_imovel_e_formatted(value[0], value[1]);

            if(value[1] == 3000000)
                return formata_valor_imovel(value[0], '', '0,00') + ' : ' + formata_valor_imovel(value[1], '', '0,00') + '+';
            else
            {
                return formata_valor_imovel(value[0] == undefined ? 0 : value[0], '', '0,00') + ' : ' + formata_valor_imovel(value[1] == undefined ? 0 : value[1], '', '0,00');
            }
        }
    });

    $(".slider-filtro").on("slide", function(slideEvt) {
        atualiza_label_no_slider_valor_imovel_e_formatted(slideEvt.value[0], slideEvt.value[1]);
    });

    // SIMULA EVENTO DE MUDANÇA PARA SELECIONAR CORRETAMENTE OS CONDOMINIOS DA CIDADE SELECIONADA
    $('.pesquisa-avancada-1 #filtro-imovel select.selectpicker[name="cidade"]').trigger("change");
});

function atualiza_label_no_slider_valor_imovel_e_formatted(valMin, valMax)
{
    $(' input[name="preco_min"]').val(valMin);
    $(' input[name="preco_max"]').val(valMax);

    var mostrar_mais = '';
    if(valMax == 3000000)
        mostrar_mais = '+';

    $('.filtro-valores').html('<span class="pull-left valor-minimo">' + formata_valor_imovel(valMin, '<small> R$</small>') +'</span><span class="pull-right">' + formata_valor_imovel(valMax, '<small>R$</small>') + mostrar_mais + '</span>');
}


function formata_valor_imovel(valor_imovel, cifrao)
{
    if(valor_imovel == 'Consulte')
        return valor_imovel;
    else
    {
        var e = $('<div></div>');
        e.autoNumeric('init', {aSep: '.', aDec: ',', vMax: 999999999.99});

        var valor = e.autoNumeric('set', valor_imovel);
        return cifrao + valor.text();
    }
}
