function bloquear_ou_desbloquear_cliente(id, element, opcao)
{
    alertify
        .logPosition("bottom right")
        .okBtn("Sim")
        .cancelBtn("Não")
        .confirm('Realmente deseja <b>' + opcao + '</b> este cliente?', function () {
        // user clicked "ok"
            var cliente =
            {
                id: id,
                bloqueado: (opcao == 'bloquear' ? 1 : 0)
            };

            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: $('#base_url').val() + 'admin/cliente/' + opcao,
                data: {id: id},
                success: function (data) {
                    if (data.status) {
                        $(element).replaceWith(cliente_monta_elemento_bloqueio(cliente));
                        alertify.success('Ação realizada com sucesso');
                    }
                    else
                        alertify.error('Erro ao ' + opcao);
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    console.log('Ocorreu um erro ao ' + opcao);
                }
            });
    }, function() {
        // user clicked "cancel"
    });
}



function cliente_monta_elemento_bloqueio(cliente)
{
    if(cliente.bloqueado == 0)
        return '<a href="#" onclick="bloquear_cliente(' + cliente.id + ', this);" title="Bloquear"><i class="fa fa-lock" style="color: #FF7531;"></i></a>';
    else
        return '<a  href="#" onclick="desbloquear_cliente(' + cliente.id + ', this);" title="Desbloquear"><i class="fa fa-unlock" style="color: #22BAA0;"></i></a>';
}

function cliente_monta_elemento_deletar(cliente)
{
    return '<a href="#" onclick="excluir_cliente(' + cliente.id + ', this);" title="Remover"><i class="fa fa-trash" style="color: red;"></i></a>';
}

function bloquear_cliente(id, element)
{
    bloquear_ou_desbloquear_cliente(id, element, 'bloquear');
}

function excluir_cliente(id, element)
{
    var tr = element.closest("tr");

    alertify
    .logPosition("bottom right")
    .okBtn("Sim")
    .cancelBtn("Não")
    .confirm('Realmente deseja <b>excluir</b> este cliente?', function () {
        // user clicked "ok"
        alertify.log('Excluindo cliente, aguarde...');

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: $('#base_url').val() + 'admin/cliente/excluir',
            data: {id: id},
            success: function (data) {
                if (data.status) {
                    tr.remove();
                    alertify.success('Ação realizada com sucesso');
                }
                else
                    alertify.error('Erro ao excluir');
            },
            error: function (jqXhr, textStatus, errorThrown) {
                alertify.error('Ocorreu um erro ao excluir');
            }
        });
    }, function() {
        // user clicked "cancel"
    });
}

function desbloquear_cliente(id, element)
{
    bloquear_ou_desbloquear_cliente(id, element, 'desbloquear');
}