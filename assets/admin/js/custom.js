	/*Mascara formulário*/

		$(".dinheiro").maskMoney({symbol:'',showSymbol:true, thousands:',', decimal:'.', symbolStay: true});
		$('.telefone').mask('(99)9999-9999?9');
		$('.telefone-whatsapp').mask('+99 99 9999-9999?9');
		$(".aniversario").mask("99/99");
		$(".ano").mask("9999");

		$("#nascimento").mask("99/99/9999");
		$("#telefone").mask("(99) 9999-9999");
		$("#celular").mask("(99) 9999-9999");
		$("#cpf").mask("999.999.999-99"); 
		$("#rg").mask("9999999999");
		$("#creci").mask("99.999");
		$("#phoneExt").mask("(999) 999-9999? x99999");
		$("#iphone").mask("+33 999 999 999");
		$("#tin").mask("99-9999999");
		$("#ssn").mask("999-99-9999");
		$("#product").mask("a*-999-a999", { placeholder: " " });
		$("#eyescript").mask("~9.99 ~9.99 999");
		$("#po").mask("PO: aaa-999-***");
		$("#pct").mask("99%");
		$("#phoneAutoclearFalse").mask("(999) 999-9999", { autoclear: false, completed:function(){alert("completed autoclear!");} });
		$("#phoneExtAutoclearFalse").mask("(999) 999-9999? x99999", { autoclear: false });
		$("input").blur(function() {
			$("#info").html("Unmasked value: " + $(this).mask());
		}).dblclick(function() {
		$(this).unmask();
	});
				
	/*Fim da mascara formulário*/

	function previewImage(input, previewId)
	{
		var preview = document.getElementById(previewId);

		if(!checkFileExtension($(input)))
		{
			preview.setAttribute('src', Math.random());
			return;
		}

		if(input.files.length > 0)
		{
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					preview.setAttribute('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			} else {
				preview.setAttribute('src', 'placeholder.png');
			}
		}
	}

	function checkFileExtension(fileUploader)
	{
		if (fileUploader.attr("accept") != "")
		{
			var fileExtension = $.map(fileUploader.attr("accept").split(","), function(value, index){
				return $.trim(value.replace('.', ''));
			});

			if ($.inArray(fileUploader.val().split('.').pop().toLowerCase(), fileExtension) == -1)
			{
				fileUploader.val("");
				alertify.error("Extensão de arquivo incorreta, neste campo apenas: " + fileExtension.join(', '));
				return false;
			}
		}

		return true;
	}

	$('.decimal').keyup(function(){
		var val = $(this).val();
		if(isNaN(val)){
			val = val.replace(/[^0-9\.]/g,'');
			if(val.split('.').length>2)
				val =val.replace(/\.+$/,"");
		}
		$(this).val(val);
	});

	$('.date-picker').datepicker({
		format: "dd/mm/yyyy",
		orientation: "top auto",
		autoclose: true,
		language: 'pt-BR'
	});

	$('.hour-picker').timepicker({
		showMeridian: false
	});

	// Element Blocking
	function blockUI(item) {
		$(item).block({
			message: '<img src="' + $('#base_url').val() + '/assets/admin/images/reload.gif" width="20px" alt="">',
			css: {
				border: 'none',
				padding: '0px',
				width: '20px',
				height: '20px',
				backgroundColor: 'transparent'
			},
			overlayCSS: {
				backgroundColor: '#fff',
				opacity: 0.9,
				cursor: 'wait'
			}
		});
	}

	function unblockUI(item) {
		$(item).unblock();
	}

	$.adminRequest = function(options){
		// This is the easiest way to have default options.
		return $.extend({
			// These are the defaults.
			successCallback: function(data){},
			failureCallback: function(data){},
			errorCallback: function(jqXhr, textStatus, errorThrown){},
			completeCallback: function(){}
		}, options );
	};

	function ajaxPost(data, url, adminRequest)
	{
		ajaxUtil(data, url, 'POST', adminRequest);
	}

	function ajaxGet(data, url, adminRequest)
	{
		ajaxUtil(data, url, 'GET', adminRequest);
	}

	function ajaxUtil(data, url, method, adminRequest)
	{
		if(adminRequest.blockElement != undefined && adminRequest.blockElement.length)
			blockUI(adminRequest.blockElement);

		$.ajax({
			url: url,
			type: method,
			dataType: "json",
			data: data,
			success: function (data) {
				if(data.status)
					adminRequest.successCallback(data);
				else
					adminRequest.failureCallback(data);
			},
			error: function (jqXhr, textStatus, errorThrown) {
				adminRequest.errorCallback(jqXhr, textStatus, errorThrown);
			},
			complete: function(){
				if($.isFunction(adminRequest.completeCallback))
					adminRequest.completeCallback();

				if(adminRequest.blockElement != undefined && adminRequest.blockElement.length)
					unblockUI(adminRequest.blockElement);
			}
		});
	}

	function obter_items_por_data_value(selector, data_value)
	{
		var valores = [];

		selector.each(function(){
			if($(this).data(data_value) != undefined)
				valores.push($(this).data(data_value))
		});

		return valores;
	}