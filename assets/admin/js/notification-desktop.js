var notifyEnable = true;

var windowIsFocus;

window.onblur = function() { windowIsFocus = false; };
window.onfocus = function() { windowIsFocus = true; };

// request permission on page load
document.addEventListener('DOMContentLoaded', function () {

    messageToEnable();

    if (Notification.permission == "granted")
        startNofity();
});

function grantNotify() {
    if (Notification.permission == "default") {
        Notification.requestPermission(function () {
            if (Notification.permission == "granted") {
                startNofity();
                nofityEnable();
            }
        });
    }
    else if(Notification.permission == 'denied')
    {
        alert('Você bloqueou ou não permitiu as notificações da página. Busque nas configurações do navegador por "Notificações" e remova dos bloqueados.');
    }
}

function startNofity()
{
    $('<audio id="notification-showing-sound"> <source src="' + notificationSound + '" type="audio/mpeg"> </audio>').appendTo('body');
}

var notificationIcon  = null;
var notificationSound = null;

var notifiyDesktopConfig = {
    focusOnClick: true,
    closeOnClick: true,
    soundOnShowing: true,
    showNotifyWindowOnFocus: false,
    onClick: function() {}
};

function notifyDesktop(title, body, config) {

    if(!notifyEnable) {//Não habilitadas
        notifyDisable();
        return;
    }

    if(config == null || config == undefined)
        config = notifiyDesktopConfig;

    if (!Notification) {
        alert('Notificações da area de trabalho não disponíveis no seu navegador. Por favor use o chrome para uma melhor experiência do sistema.');
        return;
    }
    else if(Notification.permission != 'granted'){
        notifyBlock();
        return;
    }

    if(!windowIsFocus || config.showNotifyWindowOnFocus){
        notification = new Notification(title, {
            icon: notificationIcon,
            body: body,
        });
    }

    if(config.soundOnShowing && notificationSound != null){
        $('#notification-showing-sound').attr('src', notificationSound);

        $('#notification-showing-sound')[0].play();
    }

    notification.onclick = function () {
        if(config.focusOnClick)
            window.focus();

        if(config.closeOnClick)
            notification.close();

        config.onClick();
    };
}

function nofityEnable()
{
    notifyEnable = true;
    if($('#message-enable-notification').length > 0)
        $('#message-enable-notification').show();

    if($('#message-disable-notification').length > 0)
        $('#message-disable-notification').hide();

    if($('#message-grant-notification').length > 0)
        $('#message-grant-notification').hide();
}

function notifyDisable()
{
    notifyEnable = false;
    if($('#message-disable-notification').length > 0)
        $('#message-disable-notification').show();

    if($('#message-enable-notification').length > 0)
        $('#message-enable-notification').hide();

    if($('#message-grant-notification').length > 0)
        $('#message-grant-notification').hide();
}

function notifyBlock()
{
    if(Notification.permission != 'granted' && $('#message-grant-notification').length > 0)
        $('#message-grant-notification').on('click', grantNotify).show();

    notifyEnable = false;
    if($('#message-disable-notification').length > 0)
        $('#message-disable-notification').hide();

    if($('#message-enable-notification').length > 0)
        $('#message-enable-notification').hide();
}

function messageToEnable()
{
    if($('#message-grant-notification').length > 0)
        $('#message-grant-notification').on('click', notifyBlock);

    if($('#message-enable-notification').length > 0)
        $('#message-enable-notification').on('click', notifyDisable);

    if($('#message-disable-notification').length > 0)
        $('#message-disable-notification').on('click', nofityEnable);


    //EXIBE MENSAGEM PARA HABILITAR A NOTIFICACAO
    if(Notification.permission != 'granted' && $('#message-grant-notification').length > 0)
        $('#message-grant-notification').show();
    else if(notifyEnable && $('#message-enable-notification').length > 0)
        $('#message-enable-notification').show();
    else if(!notifyEnable && $('#message-disable-notification').length > 0)
        $('#message-disable-notification').show();
}