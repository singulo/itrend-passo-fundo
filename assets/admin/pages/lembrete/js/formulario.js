var lembretes_atualizados = {
    successCallback : []
};

$(document).ready(function(){
    $('#modal-lembrete-formulario form').validate({
        rules: {
            titulo: {
                required: true
            },
            descricao: {
                required: true
            },
            dia: {
                required: true
            },
            destinatarios: {
                required: true,
                minlength: 1
            }
        }
    });

    obter_todos_corretores(
        function (data) {

            if(data.corretores.length > 0)
            {
                $.each( data.corretores, function( key, value ) {
                    $('#modal-lembrete-formulario select optgroup[data-nivel="' + value.nivel + '"]').append($('<option>', {value:value.id_corretor, text:value.nome}));
                });

                var selected = $('#modal-lembrete-formulario select.select-2').data('selected');

                $('#modal-lembrete-formulario select.select-2').select2();
                $('#modal-lembrete-formulario select.select-2').select2('val', selected);
            }
            else
            {
                $('#corretor_padrao').append('<option value="0" disabled selected>Nenhum resultado encontrado.</option>');
            }
        },
        function (jqXhr, textStatus, errorThrown) {
            alertify.error('Ocorreu um erro ao tentar obter os corretores!');
        }
    );
});

function novo_lembrete()
{
    if(!$('#modal-lembrete-formulario form').valid())
        return;

    var lembrete = $('#modal-lembrete-formulario form').jsonify();

    lembrete.dia = $.format.date($('#modal-lembrete-formulario form .date-picker').datepicker("getDate"), "yyyy-MM-dd");

    if(lembrete.destinatarios == undefined || lembrete.destinatarios.length == 0)
    {
        alertify.error('Escolha pelo menos um destinatario.');
        return;
    }

    var $btn = $('#modal-lembrete-formulario button:contains("Salvar")').button('loading');

    ajaxPost(
        lembrete,
        $('#base_url').val() + 'admin/lembrete/salvar',
        {
            successCallback: function(data){

                $.each( lembretes_atualizados.successCallback, function( key, action ) {
                    action(data);
                });

                alertify.success('Lembrete salvo com sucesso.');
                $('#modal-lembrete-formulario').modal('hide');
            },
            failureCallback: function(data){
                if(data.msg != undefined)
                    alertify.error(data.msg);
                else
                    alertify.error('Ocorreu um erro ao tentar salvar o lembrete!');
            },
            errorCallback: function(){
                alertify.error('Ocorreu um erro ao tentar salvar o lembrete. Por favor tente mais tarde.');
            },
            completeCallback: function(){
                $btn.button('reset');
            }
        }
    );
}

function abrir_lembrete(lembrete, element)
{
    if(lembrete == undefined)
    {
        $('#modal-lembrete-formulario input[name="id"]').val('');
        $('#modal-lembrete-formulario input[name="titulo"]').val('');
        $('#modal-lembrete-formulario textarea[name="descricao"]').val('');
        $('#modal-lembrete-formulario input[name="dia"]').val('');
        $('#modal-lembrete-formulario input[name="hora"]').val('');
        $('#modal-lembrete-formulario select.select-2').select2('val', $('#modal-lembrete-formulario select.select-2').data('selected'));
        $('#modal-lembrete-formulario button').hide();
        $('#modal-lembrete-formulario button:contains("Salvar")').show();
    }
    else
    {
        lembrete.dia = $.format.date(lembrete.data, "dd/MM/yyyy");
        lembrete.hora = $.format.date(lembrete.data, "HH:mm");

        if(lembrete.destinatarios == undefined)
        {
            ajaxPost(
                { id: lembrete.id },
                $('#base_url').val() + 'admin/lembrete/destinatarios',
                {
                    successCallback: function(data){
                        lembrete.destinatarios = data.destinatarios;

                        if(element != undefined)//ATUALIZA O LEMBRETE QUE ESTA NO ELEMENTO PARA NAO BUSCAR NOVAMENTE OS DESTINATARIOS
                            $(element).attr('onclick', 'abrir_lembrete(' + JSON.stringify(lembrete) + ', this);');

                        $('#modal-lembrete-formulario select.select-2').select2('val', data.destinatarios);
                    },
                    failureCallback: function(data){
                        alertify.error('Ocorreu um erro ao obter os destinatarios do lembrete.');
                    },
                    errorCallback: function(){
                        alertify.error('Ocorreu um erro ao obter os destinatarios do lembrete. Tente novamente mais tarde.');
                    },
                    blockElement: $('#modal-lembrete-formulario .modal-content')
                }
            )
        }

        $('#modal-lembrete-formulario form').dejsonify(lembrete);
        $('#modal-lembrete-formulario button').hide();
        $('#modal-lembrete-formulario button:contains("Não lembrar mais")').show();
    }

    $('#modal-lembrete-formulario').modal('show');
}

function abrir_lembrete_pelo_id(id)
{
    ajaxPost(
        { id: id },
        $('#base_url').val() + 'admin/lembrete/detalhes',
        {
            successCallback: function(data){
                abrir_lembrete(data.lembrete);
            },
            failureCallback: function(data){
                alertify.error('Ocorreu um erro ao obter os detalhes do lembrete.');
            },
            errorCallback: function(){
                alertify.error('Ocorreu um erro ao obter os detalhes do lembrete. Tente novamente mas tarde.');
            }
        }
    );
}

function marcar_lembrete_como_lido()
{
    var id_lembrete = $('#modal-lembrete-formulario form input[name="id"]').val();
    console.log(id_lembrete);
    ajaxPost(
        { id: id_lembrete },
        $('#base_url').val() + 'admin/lembrete/marcar_como_lido',
        {
            successCallback: function(data){

                $.each( lembretes_atualizados.successCallback, function( key, action ) {
                    action(data);
                });

                $('#modal-lembrete-formulario').modal('hide');
                alertify.success('Lembrete marcado como lido.');
            },
            failureCallback: function(data){
                alertify.error('Ocorreu um erro ao marcar o lembrete.');
            },
            errorCallback: function(){
                alertify.error('Ocorreu um erro ao marcar o lembrete. Tente novamente mais tarde.');
            },
            blockElement: $('#modal-lembrete-formulario .modal-content')
        }
    );
}

function obter_lembretes_nao_lidos(successCallback, completeCallback)
{
    ajaxPost(
        {},
        $('#base_url').val() + 'admin/lembrete/nao_lidos',
        {
            successCallback: function (data) {
                successCallback(data);
            },
            failureCallback: function (data) {
                alertify.error('Ocorreu um erro ao obter os lembretes não lidos.');
            },
            errorCallback: function () {
                alertify.error('Ocorreu um erro ao obter os lembretes não lidos. Tente novamente mais tarde.');
            },
            completeCallback: function (){
                completeCallback();
            }
        }
    );
}