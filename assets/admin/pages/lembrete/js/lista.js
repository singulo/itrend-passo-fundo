$(document).ready(function() {
    lembretes_ativos();

    atualizar_quantidade_lembretes();
});

lembretes_atualizados.successCallback.push(function(data){
    atualizar_quantidade_lembretes();
    if(filtro.nao_lido)
    {
        obter_lembretes(filtro);
    }
});

//Adiciona classe de ativo ao clique
$('.mailbox-nav li').on('click', function(){

    $('.mailbox-nav li').removeClass('active');

    $(this).addClass('active');
});

function lembretes_ativos()
{
    $('#pesquisar-campos-contenham').attr("placeholder", "Pesquisar em lembretes ativos...");

    obter_lembretes({pagina: 0, nao_lidos: true});
}

function lembretes_desativados()
{
    $('#pesquisar-campos-contenham').attr("placeholder", "Pesquisar em lembretes desativados...");

    obter_lembretes({pagina: 0, lidos: true});
}

function atualizar_quantidade_lembretes()
{
    $.ajax({
        url: $('#base_url').val() + 'admin/lembrete/total',
        type: 'POST',
        dataType: "json",
        success: function (total) {
            $('#quantidade-ativos').text(total.ativos);
            $('#quantidade-desativados').text(total.desativados);
            //$('#quantidade-total').text(total.geral);
        },
        error: function () {
            alertify.error('Ocorreu um erro ao tentar obter a quantidade das notificações!');
        }
    });
}

var filtro;
function obter_lembretes($filtro)
{
    filtro = $filtro;
    filtro.limite = 15; //resultados por pagina

    $("#tabela-lembretes").find('tbody').empty();

    blockUI($("#tabela-lembretes"));

    $('#info-lembretes-encontrados').text('');
    $('#btn_voltar_lembretes').attr('disabled', 'disabled');
    $('#btn_proximo_lembretes').attr('disabled', 'disabled');

    ajaxGet(
        filtro,
        $('#base_url').val() + 'admin/lembrete/pesquisar',
        {
            successCallback: function (data) {
                $("#tabela-lembretes").find('tbody').empty();

                console.log(data);

                if(data.lembretes.length > 0)
                {
                    var mostrando = 0;

                    $.each( data.lembretes, function( key, lembrete ) {
                        mostrando ++;
                        $('#tabela-lembretes').find('tbody').append('<tr onclick=\'abrir_lembrete(' + JSON.stringify(lembrete) + ', this);\'><td><img src="' + $('#fotos_corretores_url').val() + lembrete.criado_por + '.jpg" class="img-circle" onerror="this.src=\'' + $('#base_url').val() + 'assets/images/corretor-foto-padrao.png\';"></td><td class="text-left">' + lembrete.titulo + '</td><td>' + (lembrete.descricao.length > 43 ? lembrete.descricao.substr(0, 40) + '...' : lembrete.descricao) + '</td><td class="text-right">' + $.format.date(lembrete.data, "dd/MM HH:mm") + '</td></tr>');
                    });

                    $('#info-lembretes-encontrados').text('Mostrando ' + ((filtro.pagina * filtro.limite) + 1) + '-' + ((filtro.pagina * filtro.limite) + mostrando) + ' de ' + data.total);

                    if(filtro.pagina > 0)
                        $('#btn_voltar_lembretes').removeAttr('disabled');
                    if(filtro.pagina + 1 < Math.ceil(data.total / filtro.limite) && Math.ceil(data.total / filtro.limite) >= 2)
                        $('#btn_proximo_lembretes').removeAttr('disabled');
                }
                else
                {
                    $('#tabela-lembretes').find('tbody').append('<tr><td colspan="5" class="text-center">Nenhum resultado encontrado.</td></tr>');
                }
            },
            failureCallback: function (data) {
                alertify.error('Ocorreu um erro ao obter os lembretes.');
            },
            errorCallback: function (data) {
                alertify.error('Ocorreu um erro ao obter os lembretes. Tente novamente mais tarde.');
            },
            blockElement: $("#tabela-lembretes")
        });
}

function lembretes_pesquisar()
{
    filtro.campos_contenham = $('#pesquisar-campos-contenham').val();

    obter_lembretes(filtro);
}