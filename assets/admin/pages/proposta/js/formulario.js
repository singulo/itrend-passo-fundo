$(document).ready(function() {
    $('.summernote').summernote({
        height: 200,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ]
    });

    /* BUSCA OS IMOVEIS CASO SEJA UM REENVIO DE PROPOSTA */
    if($('#form-proposta [name="cod_imovel"]').val().length > 0)
        buscar_imovel();
});

function enviar_proposta()
{
    var imoveis = obter_items_por_data_value($('#tabela-imoveis tbody tr'), 'imovel');

    var mensagem = imoveis.length == 0 ? 'Você não está selecionando nenhum imóvel para enviar nesta proposta. Deseja enviar assim mesmo?' : 'Enviar sua proposta?';

    alertify
        .okBtn('Sim')
        .cancelBtn('Não')
        .confirm(mensagem, function(){
            ajaxPost(
                {
                    cliente_email : $('#form-proposta input[name="email"]').val(),
                    cliente_nome : $('#form-proposta input[name="nome"]').val(),
                    cod_imoveis : imoveis.map(function(imovel){ return imovel.f_codigo }),
                    assunto : $(".summernote").code() ,
                    copia_remetente: $("#copia_remetente").prop('checked')
                },
                $('#base_url').val() + 'admin/proposta/enviar',
                {
                    successCallback: function (data) {
                        alertify.success('Proposta enviada com sucesso');
                    },
                    failureCallback: function(data){
                        alertify.error(data.msg);
                        alertify.log('Dica: Use o botão "Visualizar proposta" para ver como sua proposta vai ser entregue ao destinatário');
                    },
                    errorCallback: function (jqXhr, textStatus, errorThrown) {
                        alertify.error('Ocorreu um erro ao enviar a proposta');
                    },
                    blockElement: $("#form-proposta")
                }
            );
        });
}

function visualizar_proposta()
{
    var imoveis = obter_items_por_data_value($('#tabela-imoveis tbody tr'), 'imovel');
    if(imoveis.length == 0)
        alertify.log('Você não adicionou nenhum imóvel para a proposta.');
    else
        window.open($('#base_url').val() + 'admin/proposta/visualizar?cliente_nome=' + $('#form-proposta input[name="nome"]').val() + '&cliente_email=' + $('#form-proposta input[name="email"]').val() + '&assunto=' + $(".summernote").code() + '&cod_imoveis=' +  obter_items_por_data_value($('#tabela-imoveis tbody tr'), 'imovel').map(function(imovel){ return imovel.f_codigo }), '_blank');
}

function buscar_imovel()
{
    var $btn = $('#btn-busca-imovel').button('loading');

    ajaxGet(
        {cod_imoveis: $('#form-proposta [name="cod_imovel"]').val().split(',')},
        $('#base_url').val() + 'admin/imovel/pelo_codigo',
        {
            successCallback: function(data){
                //REMOVE INFORMAÇÃO ÍNICIAL COM A TABELA
                $('#tabela-imoveis tbody tr td[colspan="4"]').remove();

                $('#form-proposta [name="cod_imovel"]').val(''); //LIMPA CAMPO

                var imovel_ja_add;
                $.each(data.imoveis, function(index, imovel){

                    imovel_ja_add = false;

                    $.each(obter_items_por_data_value($('#tabela-imoveis tbody tr'), 'imovel'), function(ind, imvl){
                        if(imvl.f_codigo == imovel.f_codigo)
                            imovel_ja_add = true;
                    });

                    if(!imovel_ja_add)
                        $('#tabela-imoveis tbody').append('<tr data-imovel=\'' + JSON.stringify(imovel) + '\'><td><a href="' + $('#base_url').val() + 'imovel?cod_imovel=' + imovel.f_codigo + '" target="_blank">' + imovel.f_codigo + '</a></td><td>' + imovel.f_tipo + '</td><td>' + imovel.f_cidade + '</td><td>' + exibir_valor_imovel(imovel.f_valor, 'R$ ') + '</td><td><a href="#" onclick="this.closest(\'tr\').remove();" title="Remover"><i class="fa fa-trash" style="color: red;"></i></a></td></tr>');
                    else
                        alertify.log('Imóvel ' + imovel.f_codigo + ' já está na lista');

                });
            },
            failureCallback: function(data) {
                alertify.error(data.msg);
            },
            errorCallback: function() {
                alertify.error('Ocorreu um erro ao obter o imóvel. Tente novamente mais tarde!');
            },
            completeCallback: function(){
                $btn.button('reset');
            }
        }
    );
}