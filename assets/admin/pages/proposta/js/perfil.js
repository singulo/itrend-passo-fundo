$(document).ready(function() {

    var $validator = $("#wizardForm").validate({
        rules: {
            'cidades[]' : {
                required: true
            },
            'tipos[]' : {
                required: true
            }
        },
        messages: {
            'clientes[]': {
                required: "Selecione pelo menos 1 cliente",
                maxlength: "Selecione no máximo {0} clientes"
            }
        }
    });

    $('#rootwizard').bootstrapWizard({
        'tabClass': 'nav nav-tabs',
        onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = ($current/$total) * 100;
            $('#rootwizard').find('.progress-bar.progress-bar-success').css({width:$percent+'%'});

            if($current == $total) //ABA FINALIZAR
                prepara_aba_finalizar();
        },
        'onNext': function(tab, navigation, index) {
            var $valid = $("#wizardForm").valid() && eval($(tab).data('function-valid'));
            if(!$valid) {
                $validator.focusInvalid();
                return false;
            }
        },
        'onTabClick': function(tab, navigation, index, clickedIndex, $clickedTab) {
            // DESABILITADO POR SEGURANÇA
            return false;
        }
    });
});

function prepara_aba_finalizar()
{
    $('#finalizar .msg-enviado').text('Enviado 0 de ' + $clientes_selecionados.length);
    $('ul.pager.wizard > li').hide();
}

var $clientes_selecionados = [];
var $imoveis_selecionados = [];

$(document).ready(function() {

    //CARREGA CIDADES E TIPOS
    $.each(cidades_imoveis, function (i, item) {
        $('#buscar-perfil [name="cidades[]"]').append($('<option>', {
            value: item.f_cidade,
            text: item.f_cidade
        }));
    });

    $('#buscar-perfil [name="cidades[]"]').select2({maximumSelectionLength: 2});

    $.each(tipos_imoveis, function (i, item) {
        $('#buscar-perfil [name="tipos[]"]').append($('<option>', {
            value: item.f_tipo,
            text: item.f_tipo
        }));
    });

    $('#buscar-perfil [name="tipos[]"]').select2({maximumSelectionLength: 2});

    //MENSAGEM
    $('.summernote').summernote({
        height: 200,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ],
        // onChange: atualiza_resposta_email
    });
});

var filtro;

function buscar()
{
    filtro = monta_objeto_busca();

    if(!$("#wizardForm").valid() &&
        ((filtro.cidades == null || filtro.cidades.length == 0) ||
        (filtro.tipos == null || filtro.tipos.length == 0)))
        return;

    var $btn = $('#buscar-perfil [type="button"]').button('loading');

    ajaxPost(
        filtro,
        $('#base_url').val() + 'admin/proposta/perfil_filtro',
        {
            successCallback: function(data) {

                /* ZERA SELECIONADOS DO FILTRO ANTERIOR */
                $clientes_selecionados = [];
                $imoveis_selecionados = [];

                /* CLIENTES */
                $('#tabela-clientes tbody').html('');

                $.each(data.clientes, function(index, cliente){
                    $('#tabela-clientes tbody').append('<tr data-cliente=\'' + JSON.stringify(cliente) + '\'><td><input type="checkbox"></td><td>' + cliente.nome + '</td><td>' + cliente.email + '</td><td>' + cliente.telefone_1 + '</td></tr>');
                });

                //ADICIONA EVENTO AO CHECKBOX MUDAR
                $('#tabela-clientes :checkbox').change( function() {
                    $clientes_selecionados = obter_items_por_data_value($('#tabela-clientes tbody tr').filter(':has(:checkbox:checked)'), 'cliente');
                });

                /* IMOVEIS */
                $('#tabela-imoveis tbody').html('');

                if(data.imoveis.length == 0)
                {
                    $('#tabela-imoveis tbody').append('<tr><td colspan="4" class="text-center">Nenhum imóvel foi encontrado para o perfil que foi buscado.</td></tr>');
                }
                else {
                    $.each(data.imoveis, function(index, imovel) {
                        $('#tabela-imoveis tbody').append('<tr data-imovel=\'' + JSON.stringify(imovel) + '\'><td><input type="checkbox"></td><td><a href="' + $('#base_url').val() + 'imovel?cod_imovel=' + imovel.f_codigo + '" target="_blank">' + imovel.f_codigo + '</a></td><td>' + imovel.f_tipo + '</td><td>' + imovel.f_cidade + '</td><td>' + exibir_valor_imovel(imovel.f_valor, 'R$ ') + '</td></tr>');
                    });

                    //ADICIONA EVENTO AO CHECKBOX MUDAR
                    $('#tabela-imoveis :checkbox').change( function() {
                        $imoveis_selecionados = obter_items_por_data_value($('#tabela-imoveis tbody tr').filter(':has(:checkbox:checked)'), 'imovel');
                    });
                }
            },
            failureCallback: function(data) {
                alertify.error('Ocorreu um erro ao obter os clientes');
            },
            errorCallback: function() {
                alertify.error('Ocorreu um erro ao obter os clientes. Tente novamente mais tarde!');
            },
            completeCallback: function(){
                $btn.button('reset');
            },
            blockElement:  $('#tabela-clientes tbody')
        }
    )
}

function monta_objeto_busca()
{
    return {
        cidades: $('#buscar-perfil [name="cidades[]"]').val(),
        tipos: $('#buscar-perfil [name="tipos[]"]').val(),
        valor_min: $('#buscar-perfil [name="valor_min"]').val().replace('R$ ', '').replace(',', ''),
        valor_max: $('#buscar-perfil [name="valor_max"]').val().replace('R$ ', '').replace(',', '')
    };
}

function obter_items_por_data_value(selector, data_value)
{
    var selecionados = [];

    trSelecionadas = selector;

    trSelecionadas.each(function(){
        if($(this).data(data_value) != undefined)
            selecionados.push($(this).data(data_value))
    });

    return selecionados;
}

function clientes_selecionados()
{
    return $clientes_selecionados.length > 0 && $clientes_selecionados.length <= 10;
}

function imoveis_adicionados()
{
    return $imoveis_selecionados.length > 0 && $imoveis_selecionados.length <= 5;
}

function mensagem_valida()
{
    var tamanho = $.trim($(".summernote").code()).length;

    return tamanho > 4 && tamanho <= 350;
}

function ver_email()
{
    var imoveis = [];
    $.each($imoveis_selecionados, function(index, imovel){
        imoveis.push(imovel.f_codigo);
    });

    window.open($('#base_url').val() + 'admin/proposta/visualizar?cliente_nome=' + $clientes_selecionados[0].nome + '&cliente_email=' + $clientes_selecionados[0].email + '&assunto=' + $(".summernote").code() + '&cod_imoveis=' + imoveis, '_blank');
}

function enviar_propostas()
{
    var enviadas_com_sucesso = 0;

    var $btn = $('#finalizar [type="button"]').button('loading');
    informacoes_propostas_enviadas(enviadas_com_sucesso);

    var imoveis = [];
    $.each($imoveis_selecionados, function(index, imovel){
        imoveis.push(imovel.f_codigo);
    });

    $.each($clientes_selecionados, function(index, cliente){

        var proposta_entregue = false;

        $.ajax({
            url : $('#base_url').val() + 'admin/proposta/enviar',
            type : 'POST',
            dataType: "json",
            data: {
                cliente_email : cliente.email,
                cliente_nome : cliente.nome,
                cod_imoveis : imoveis,
                assunto : $(".summernote").code(),
                copia_remetente: $("#copia_remetente").prop('checked')
            },
            success: function (data) {
                if(data.status)
                {
                    proposta_entregue = true;
                    enviadas_com_sucesso = enviadas_com_sucesso + 1;
                    informacoes_propostas_enviadas(enviadas_com_sucesso);
                }
                else
                    alertify.error(data.msg);
            },
            error: function (jqXhr, textStatus, errorThrown) {
                alertify.error('Ocorreu um erro ao enviar a proposta');
            },
            complete: function(){
                if(!proposta_entregue)
                    return;

                if(index + 1 == $clientes_selecionados.length) {
                    $btn.text('Fim');

                    if (enviadas_com_sucesso == $clientes_selecionados.length)
                        alertify.success('Propostas enviadas com sucesso!');
                    else
                        alertify.error('Ocorreu um erro ao enviar as propostas');

                    $('ul.pager.wizard > li.finish').removeClass('hidden').show();
                }
            }
        });
    });
}

function informacoes_propostas_enviadas(quantidade)
{
    $('#finalizar .msg-enviado').text('Enviado ' + quantidade + ' de ' + $clientes_selecionados.length);
    $('#finalizar .progress-bar-striped').css({width: ((quantidade / $clientes_selecionados.length) * 100 )  + '%'});
}