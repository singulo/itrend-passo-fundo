$(document).ready(function(){
    corretor_envia_clientes_muda();

    $validator = $("#form-transferir-clientes").validate({
        rules: {
            corretor_envia_clientes: {
                required: true
            },
            corretor_recebe_clientes: {
                required: true
            }
        },
        messages: {
            corretor_recebe_clientes: {
                required: "Selecione um corretor diferente de " + $('#corretor_envia_clientes option:selected').text() + "."
            }
        }
    });
});

$('#form-transferir-clientes select').on('change', function(){

    $(this).closest('.form-group').find('img').attr('src', $('#fotos_corretores_url').val() + $(this).val() + '.jpg');
});

function corretor_envia_clientes_muda()
{
    if($('#corretor_recebe_clientes option:disabled').length)
        $('#corretor_recebe_clientes option:disabled')[0].disabled = false;

    $('#corretor_recebe_clientes option[value="' + $('#corretor_envia_clientes').val() + '"]')[0].disabled = true;
}

function transferir_clientes()
{
    if(!$("#form-transferir-clientes").valid())
        return;

    alertify
        .okBtn('Sim')
        .cancelBtn('Não')
        .confirm('Você tem certeza que deseja transferir os clientes de ' + $('#corretor_envia_clientes option:selected').text() + ' para ' + $('#corretor_recebe_clientes option:selected').text() + '?', function(){
            ajaxPost(
                $("#form-transferir-clientes").jsonify(),
                $('#base_url').val() + 'admin/cliente/transferir_clientes_de_corretor',
                {
                    successCallback: function(data){
                        alertify.success('Clientes transferidos com sucesso');
                    },
                    failureCallback: function(data){
                        alertify.error('Ocorreu um erro ao transferir os clientes de corretor. Certifique-se que o corretor tem algum cliente vinculado.');
                    },
                    errorCallback: function(){
                        alertify.error('Ocorreu um erro ao transferir os clientes de corretor. Tente novamente mais tarde.');
                    },
                    blockElement: $("#form-transferir-clientes")
                }
            );
        });
}