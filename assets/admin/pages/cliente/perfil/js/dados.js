$(document).ready(function(){
    $validator = $('#form-dados').validate({
        ignore: []
    });

    //CARREGA CIDADES E TIPOS
    $.each(cidades_imoveis, function (i, item) {
        $('#form-dados [name="interesse_cidade"]').append($('<option>', {
            value: item.f_cidade,
            text : item.f_cidade
        }));
    });

    $('#form-dados [name="interesse_cidade"]').select2();

    $.each(tipos_imoveis, function (i, item) {
        $('#form-dados [name="interesse_tipo"]').append($('<option>', {
            value: item.f_tipo,
            text : item.f_tipo
        }));
    });

    $('#form-dados [name="interesse_tipo"]').select2();

    //CARREGA PERFIL
    if(cliente != null)
    {
        cliente.id_corretor_original = $('#form-dados [name="id_corretor_original"]').val();

        $('#form-dados').dejsonify(cliente);

        if(cliente.interesse_tipo != null)
            $('#form-dados [name="interesse_tipo"]').val(cliente.interesse_tipo.split(','));
        if(cliente.interesse_cidade != null)
            $('#form-dados [name="interesse_cidade"]').val(cliente.interesse_cidade.split(','));
    }

    $('select[name="id_corretor"]').trigger("change");

    if($('select[name="id_corretor"]').is(':disabled'))
        $('select[name="id_corretor"]').hide();
});

$('#form-dados select[name="id_corretor"]').on('change', function(){
    var corretor = $('select[name="id_corretor"] option:selected').data('corretor');

    $('#form-dados img').attr('src', $('#fotos_corretores_url').val() + corretor.id_corretor + '.jpg');
    $('#form-dados .corretor-nome').text(corretor.nome);
    $('#form-dados .corretor-creci').text(corretor.creci);
    $('#form-dados .corretor-email').text(corretor.email);
});

function salvar_dados_cliente()
{
    var $valid = $('#form-dados').valid();

    if(!$valid) {
        $validator.focusInvalid();
        return false;
    }
    else {

        var $btn = $('#form-dados button[type="button"]').button('loading');

        var cliente = $('#form-dados').jsonify();
        cliente.valor_imovel = Number(cliente.valor_imovel.replace(/[^0-9\.]+/g,""));
        cliente.interesse_valor = Number(cliente.interesse_valor.replace(/[^0-9\.]+/g,""));
        cliente.interesse_entrada = Number(cliente.interesse_entrada.replace(/[^0-9\.]+/g,""));

        ajaxPost(
            cliente,
            $('#base_url').val() + 'admin/cliente/salvar_perfil',
            {
                successCallback: function (data) {

                    alertify.success('Perfil salvo com sucesso.');

                    setTimeout(function (){
                        window.location.href = $('#base_url').val() + 'admin/cliente/lista'
                    }, 500);
                },
                failureCallback: function(data){
                    if(data.msg != undefined)
                        alertify.error(data.msg);
                    else
                        alertify.error('Ocorreu um erro ao tentar salvar o perfil do cliente!');
                },
                errorCallback: function (request, error) {
                    alertify.error('Ocorreu um erro ao tentar salvar o perfil do cliente!');
                },
                completeCallback: function(){
                    $btn.button('reset');
                },
                blockElement: $('#tab-dados .col-md-10')
            }
        );
    }
}
