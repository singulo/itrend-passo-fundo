var imovelCollapseCompleto;
var ultimo_imovel_visualizado_em;

$(document).ready(function(){
    imovelCollapseCompleto = $('#collapse-completo').clone();
    ultimo_imovel_visualizado_em = $.format.date(new Date(), "yyyy-MM-dd 00:00:01");

    obter_imoveis_visualizados(ultimo_imovel_visualizado_em, null, 'Hoje');

    var ontem = new Date();
    ontem.setDate(ontem.getDate() - 1);
    obter_imoveis_visualizados(null, $.format.date(ontem, "yyyy-MM-dd 23:59:59"), '');
});

setInterval(function()
{
    obter_imoveis_visualizados($.format.date(ultimo_imovel_visualizado_em, "yyyy-MM-dd HH:mm:ss"), null, 'Hoje');

}, 5000);

function obter_imoveis_visualizados(apartir_de, ate, cabecalho_text)
{
    //alertify.maxLogItems(1).log('Carregando...');

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: $('#base_url').val() + 'admin/cliente/imoveis_visualizados_historico',
        data: {
            email : cliente.email,
            apartir_de: apartir_de,
            ate: ate
        },
        success: function(data) {
            monta_collapse(data.imoveis, cabecalho_text);

            if(cabecalho_text == 'Hoje' && data.imoveis.length > 0)
                ultimo_imovel_visualizado_em = data.imoveis[0].data_visualizacao;
        },
        error: function(){
            alertify.error('Ocorreu um erro ao tentar obter os imóveis visualizados pelo cliente!');
        }
    });
}

function monta_collapse(imoveis, cabecalho_text)
{
    $.each(imoveis, function (key, imovel) {

        if($('#heading-' + imovel.dia_visualizacao).length == 0) {
            var dia = $.format.date(imovel.data_visualizacao, 'dd/MM/yyyy');

            if(cabecalho_text != '')
                dia = cabecalho_text;

            $('#accordion').append('<div class="panel-heading" role="tab" id="heading-' + imovel.dia_visualizacao + '"><a data-toggle="collapse" data-parent="#accordion" href="#collapse-' + imovel.dia_visualizacao + '" aria-expanded="true" aria-controls="collapseOne"><h4 class="panel-title">' + dia + '</h4></a></div>');

            $('#accordion').append('<div id="collapse-' + imovel.dia_visualizacao + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-' + imovel.dia_visualizacao + '"><div class="panel-body"></div></div>');
        }

        $('#collapse-' + imovel.dia_visualizacao).find('.panel-body').append('<div class="message-attachment"><a href="' + $('#base_url').val() + 'imovel?cod_imovel=' + imovel.f_codigo + '" target="_blank"><img src="' + $('#fotos_imoveis_url').val() + imovel.foto + '.jpg" onError="this.src=$(\'#base_url\').val() + \'assets/images/imovel-sem-foto.jpg\';"><div class="attachment-info"><div class="row"><div class="col-xs-9"><p>' + exibir_valor_imovel(imovel.f_valor, 'R$') + '</p><span>' + imovel.f_codigo + '</span><br><span>' + imovel.f_tipo + '</span><br><span>' + imovel.f_cidade + '</span></div><div class="col-xs-3"><div class="row"><span class="pull-left fa fa-clock-o"> ' + $.format.date(imovel.data_visualizacao, 'H:m') + '</span></div></div></div></div></a></div>');
    });
}