$(document).ready(function() {
    //turn to inline mode
    $.fn.editable.defaults.mode = 'popup';

    //editables
    $('#example-editable td a').editable({
        title: 'Meta atual do corretor',
        ajaxOptions: {
            dataType: 'json' //assuming json response
        },
        //inputclass: 'dinheiro',
        success: function(response, newValue) {

            if(response.status === false)
            {
                alertify.error('Ocorreu um erro ao atualizar a meta do corretor.');
                return ''; //msg will be shown in editable form
            }
            else
            {
                var progresso = (newValue * 100) / $('#valor_meta_individual').val();

                var progressoWidth = progresso > 100 ? 100 : progresso;

                $(trCorretorClick.children()[1]).html('<div class="progress" style="margin-bottom: 0px;"><div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: ' + progressoWidth + '%;">' + progresso + '%</div></div>');

                alertify.success('Meta atualizada.');
            }
        }
    });

    $('#meta_equipe').editable({
        mode: 'inline',
        ajaxOptions: {
            dataType: 'json'
        },
        success: function(response, newValue) {
            if(response.status === false)
            {
                alertify.error('Ocorreu um erro ao atualizar a meta.');
                return ''; //msg will be shown in editable form
            }
            else
            {
                var progresso = (parseFloat(newValue) * 100) / $('#valor_meta_equipe').val();

                var progressoWidth = progresso > 100 ? 100 : progresso;

                //ATUALIZA VALOR ATUAL DA META
                $('#valor_meta_equipe').val(newValue);

                $('.meta_equipe')[0].style.width = progressoWidth + '%';
                $('.meta_equipe').html(progresso.toFixed(2) + '%');

                alertify.success('Meta de equipe atualizada.');
            }
        }
    });
});

var trCorretorClick;
$('#example-editable td a').on('click', function(element){
    trCorretorClick = $(this).closest('tr');
});