$(document).ready(function(){
    atualizar_lembretes();
});

lembretes_atualizados.successCallback.push(function(data){
    atualizar_lembretes();
});

function atualizar_lembretes()
{
    blockUI($('.lembretes .panel-body'));

    obter_lembretes_nao_lidos(
        function(data){

            $('.lembretes .panel-body div.inbox-widget.slimscroll').html('');

            $.each(data.lembretes, function(index, lembrete){
                $('.lembretes .panel-body div.inbox-widget.slimscroll').append('<a href="#" onclick=\'abrir_lembrete(' + JSON.stringify(lembrete) + ', this);\'><div class="inbox-item"><div class="inbox-item-img"><img src="' + $('#fotos_corretores_url').val() + lembrete.criado_por + '.jpg" class="img-circle" onerror="this.src=\'' + $('#base_url').val() + 'assets/images/corretor-foto-padrao.png\';"></div><p class="inbox-item-author">' + lembrete.titulo + '</p><p class="inbox-item-text">' + (lembrete.descricao.length > 43 ? lembrete.descricao.substr(0, 40) + '...' : lembrete.descricao) + '</p><p class="inbox-item-date">' + $.format.date(lembrete.data, "dd/MM HH:mm") + '</p></div></a>');
            });
        },
        function(){
            unblockUI($('.lembretes .panel-body'));
        }
    );
}