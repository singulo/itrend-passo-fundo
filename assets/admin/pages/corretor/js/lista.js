function obter_corretores(pagina, limite){

    ajaxGet(
        {
            pagina: pagina,
            limite: limite
        },
        $('#base_url').val() + 'admin/corretor/lista',
        {
            successCallback: function (data) {

                $("#tabela-corretores").find('tbody').empty();

                if(data.corretores.length > 0)
                {
                    $.each( data.corretores, function( key, value ) {
                        $('#tabela-corretores').find('tbody').append('<tr onclick=\'abre_modal_detalhe_corretor(' + JSON.stringify(value) + ');\'><td>' + value.nome + '</td><td>' + value.email + '</td><td>' + value.celular + '</td><td>' + value.total_clientes + '</td></tr>');
                    });
                }
                else
                    $('#tabela-corretores').find('tbody').append('<tr><td colspan="4" class="text-center">Nenhum resultado encontrado.</td></tr>');
            },
            errorCallback: function (request, error) {
                alertify.error('Ocorreu um erro ao tentar obter os corretores!');
            },
            blockElement: $("#tabela-corretores")
        });
}


$(document).ready(function(){
    obter_corretores(0,10);

    $(function() {
        $('#pagination').pagination({
            items: $('#corretores_total').val(),
            itemsOnPage: 10,
            cssStyle: 'light-theme',
            nextText: 'Próximo',
            prevText: 'Anterior',
            onPageClick : function(pageNumber){
                obter_corretores((pageNumber-1), 10);
            }
        });
    });
});

function abre_modal_detalhe_corretor(corretor)
{
    console.log(corretor);

    $('#modal-detalhe-corretor img').attr('src', $('#fotos_corretores_url').val() + corretor.id_corretor + '.jpg');
    $('#modal-detalhe-corretor .dados').html('<p>' + corretor.nome + '</p>');
    $('#modal-detalhe-corretor .dados').append('<p>' + corretor.email + '</p>');

    if(corretor.creci != null)
        $('#modal-detalhe-corretor .dados').append('<p>creci: ' + corretor.creci + '</p>');
    if(corretor.telefone != null)
        $('#modal-detalhe-corretor .dados').append('<p>' + corretor.telefone + '</p>');
    if(corretor.celular != null)
        $('#modal-detalhe-corretor .dados').append('<p>' + corretor.celular + '</p>');

    $('#modal-detalhe-corretor .info-box .icon-users').closest('.panel-body').find('p.counter').text(corretor.total_clientes);
    $('#modal-detalhe-corretor .info-box .icon-cursor').closest('.panel-body').find('p.counter').text(corretor.total_propostas);

    $('#modal-detalhe-corretor .modal-footer a').attr('href', $('#base_url').val() + 'admin/corretor/formulario?id=' + corretor.id_corretor);

    $('#modal-detalhe-corretor').modal('show');
}
