function obter_resultado_pesquisa(filtro, simpleRequest)
{
    ajaxPost(
        filtro,
        $('#base_url').val() + 'imovel/buscar',
        simpleRequest
    );
}

function finalidade_util(finalidade)
{
    var fin = {class : '', descricao : ''};

    switch(finalidade)
    {
        case '1':
            fin.class 	= 'sale';
            fin.descricao = 'À venda';
            break;
        case '2':
            fin.class 	= 'rent';
            fin.descricao = 'Aluguel';
            break;
    }

    return fin;
}

function registrar_visualizacao_imovel(cod)
{
    $.ajax({

        url : $('#base_url').val() + 'imovel/registrar_visualizacao_imovel',
        type : 'POST',
        data : {cod_imovel : cod},
        dataType: "json",
        success: function (data) {
            if(data.status)
                console.log('Registrada visualização de imóvel com sucesso');
            else
                console.log('Falha ao registrar visualização de imóvel');
        },
        error: function (jqXhr, textStatus, errorThrown) {
            console.log('Falha ao registrar visualização de imóvel');
        }
    });
}

//function exibir_valor_imovel(valor, cifrao, zero)
//{
//    if(    valor == 1.00
//        || valor == 0.00
//        || valor == '1,00'
//        || valor == '0,00'
//    )
//        return zero == undefined ? 'Consulte' : zero;
//    else {
//        var e = $('<div></div>');
//        e.autoNumeric('init', {aSep: '.', aDec: ',', vMax: 999999999.99});
//
//        var valor = e.autoNumeric('set', valor);
//        return cifrao + valor.text();
//    }
//}