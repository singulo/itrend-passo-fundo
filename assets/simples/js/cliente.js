var on_cliente_entrar = {
    successCallback : [],
    failureCallback : [],
    errorCallback : []
};

function cliente_verifica_email(email, successCallback, failureCallback, errorCallback, completeCallback)
{
    //Fazer requisicao, checar o email e setar a sessao
    $.ajax({
        url : $('#base_url').val() + 'cliente/login',
        type : 'POST',
        data : { email : email},
        dataType: "json",
        success : function(data) {

            if(data.status == true)
            {//já esta cadastrado
                $.each( on_cliente_entrar.successCallback, function( key, action ) {
                    action(data.usuario);
                });

                successCallback(data.usuario);
            }
            else
            {
                $.each( on_cliente_entrar.failureCallback, function( key, action ) {
                    action(data);
                });

                failureCallback(data);
            }
        },
        error : function(jqXhr, textStatus, errorThrown)
        {
            $.each( on_cliente_entrar.errorCallback, function( key, action ) {
                action(data);
            });

            errorCallback(jqXhr, textStatus, errorThrown);
        },
        complete: function () {
            completeCallback();
        }
    });
}

function cliente_registrar(cliente, simpleRequest)
{
    ajaxPost(
        cliente,
        $('#base_url').val() + 'cliente/novo',
        simpleRequest
    );
}

function cliente_atualizar_dados(cliente, simpleRequest)
{
    ajaxPost(
        cliente,
        $('#base_url').val() + 'cliente/atualizar',
        simpleRequest
    );
}

function cliente_contato_novo(contato, simpleRequest)
{
    ajaxPost(
        contato,
        $('#base_url').val() + 'contato/novo',
        simpleRequest
    );
}

function cliente_interesse_em_imovel(cod_imovel, obs, simpleRequest)
{
    ajaxPost(
        {cod_imovel : cod_imovel, obs : obs},
        $('#base_url').val() + 'contato/interesse_imovel',
        simpleRequest
    );
}