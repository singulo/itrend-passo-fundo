<?php

class PropertyRequiredNotFound extends Exception {}

/**
 * v1.2
 */
class RequestMapper
{
    /**
     * @param array $source
     * @param array $mapper
     * @param $obj
     * @param string $modelPrefix Utilizado para obter da fonte de dados a propriedade correta para setar o $obj
     * @return mixed
     * @throws PropertyRequiredNotFound
     */
    public static function parseToObject(Array $source, Array $mapper, $obj, $modelPrefix = '')
    {
        if($modelPrefix == '')
        {
            $modelPrefix = strtolower(get_class($obj));
        }

        foreach (get_class_vars(get_class($obj)) as $property => $value)
        {
            if(isset($source[$property]))
            {
                $obj->$property = self::setValue($mapper, $property, $source[$property]);
            }
            else if(isset($mapper[$property]['property']))
            {
                $obj->$property = self::setValue($mapper, $property, $source[$mapper[$property]['property']]);
            }
            else if(isset($source[$modelPrefix . '_' . $property]))
            {
                $obj->$property = self::setValue($mapper, $property, $source[$modelPrefix . '_' . $property]);
            }
            else if(isset($mapper[$property]['default_value']))
            {
                $obj->$property = self::setValue($mapper, $property, $mapper[$property]['default_value']);
            }

            if(isset($mapper[$property]['required']) && $mapper[$property]['required'] === TRUE)
                throw new PropertyRequiredNotFound("Property $property is required");
        }

        return $obj;
    }

    protected static function setValue(Array $mapper,  $property, $valueOnSource)
    {
        if(isset($mapper[$property]['handle']))
            return $mapper[$property]['handle']($valueOnSource);
        else
            return $valueOnSource;
    }
}