<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH."../modules/simples/core/Base_Controller.php";
require_once APPPATH . '../modules/simples/helpers/cliente_helper.php';
require_once APPPATH . '../modules/simples/libraries/RequestMapper.php';

/**
 * @property CI_Session $session
 * @property Clientes_Model $clientes_model
 * @property Corretores_Model $corretores_model
 */
class Base_Cliente_Controller extends Base_Controller {

	public function login()
	{
		$resposta = array(
			'status' => false
		);

		if(isset($_POST['email'])) {
			$this->load->model('simples/clientes_model');
			$cliente = $this->clientes_model->pelo_email($_POST['email']);

			if($cliente != null)
			{
				/**	@var Clientes_Model $cliente **/
				$cliente->ultimo_acesso  = date("Y-m-d H:i:s");
				$this->clientes_model->registrar_ultimo_acesso($cliente->id, $cliente->ultimo_acesso);

				$this->load->model('simples/corretores_model');
				$cliente->corretor = $this->corretores_model->obter_informacoes_basicas($cliente->id_corretor);

				if($cliente->bloqueado || $cliente->excluido)
				{
					$resposta['status'] = false;
					$resposta['msg'] = $cliente->nome . ', seu cadastro parece não estar ativo. Por favor entre em contato para saber mais!';
				}
				else
				{
					$this->session->set_userdata('usuario', $cliente);

					$resposta['usuario'] = $cliente;
					$resposta['status'] = true;
				}
			}
		}

		echo json_encode($resposta);
	}

	public function logout()
	{
		$this->session->unset_userdata('usuario');

		$this->load->helper('url');

		redirect(base_url());
	}

	public function novo()
	{
		$resposta = array(
			'status' => false
		);

		$resposta['status'] = registra_cliente($_POST, $_SESSION['filial']['corretor_padrao_id']);

		echo json_encode($resposta);
	}

	public function atualizar()
	{
		/** @var Clientes_Model $cliente */
		$cliente = $this->session->userdata('usuario');

		$cliente->nome 			 = $_POST['nome'];
		$cliente->telefone_1 	 = $_POST['telefone_1'];
		$cliente->cidade 		 = $_POST['cidade'];
        $cliente->uf             = $_POST['uf'];
		$cliente->atualizado_em  = date("Y-m-d H:i:s");
		$cliente->atualizado_por = $cliente->email;

		$this->load->model('simples/clientes_model');

		$cliente = RequestMapper::parseToObject((array)$cliente, array(), new ClienteDomain());

		echo json_encode(array('status' => $this->clientes_model->editar($cliente) > 0));
	}
}
