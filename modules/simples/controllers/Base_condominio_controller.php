<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(MODULESPATH . 'simples/core/Base_Controller.php');
//require_once(APPPATH . '../modules/simples/libraries/Watermark.php');
//require_once MODULESPATH . 'simples/helpers/cliente_helper.php';


/**
 * @property Condominio_Model $condominio_model
 * @property Imovel_Model $imovel_model
 * @property CI_Session $session
 */
class Base_Condominio_Controller extends Base_Controller
{
	protected $quantidade_fotos_principais = 3;
	protected $quantidade_sugestoes = 6;

	public function __construct()
	{
		parent::__construct();

		$this->load->model('simples/condominio_model');
	}

	public function index()
	{
		$data = array();

		if(isset($_GET['id']))
		{
			$data['condominio'] = $this->condominio_model->pelo_codigo($_GET['id']);

			if(!is_null($data['condominio']))
			{
				$this->load->model('simples/imovel_model');
				$this->load->library('simples/EmpreendimentoImagemTipos');
				$data['condominio']->fotos['normais'] = array();
				$data['condominio']->fotos['plantas'] = array();

				$fotos = $this->condominio_model->fotos($_GET['id']);
				foreach($fotos as $foto)
				{
					if(EmpreendimentoImagemTipos::Normal == $foto->tipo)
						$data['condominio']->fotos['normais'][] = $foto;
					else if(EmpreendimentoImagemTipos::Planta == $foto->tipo)
						$data['condominio']->fotos['plantas'][] = $foto;
				}

				$data['condominio']->fotos_principais = array();
				for ($i = 0; $i < $this->quantidade_fotos_principais; $i++)
				{
					if(isset($fotos[$i]))
						$data['condominio']->fotos_principais[] = $fotos[$i];
				}

				$data['condominio']->complementos = $this->condominio_model->complementos($_GET['id']);
				$data['condominio']->unidades = $this->condominio_model->unidades($_GET['id']);
				$data['condominio']->videos = $this->condominio_model->videos($_GET['id']);
				$data['condominio']->imoveis = $this->imovel_model->pelo_condominio($_GET['id']);
			}

		}

		return $data;
	}

	public function todos_fechados()
	{
		$data['condominios'] = $this->condominio_model->todos_fechados();
		return $data;
	}

	public function todos_lancamentos()
	{
		$data['condominios'] = $this->condominio_model->todos_lancamentos();
		return $data;
	}
}