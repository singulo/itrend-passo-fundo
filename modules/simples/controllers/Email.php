<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH."../modules/simples/core/Base_Controller.php";
require_once APPPATH . '../modules/simples/helpers/cliente_helper.php';
require_once APPPATH . '../modules/simples/helpers/email_helper.php';

/**
 * @property CI_Session $session
 * @property Clientes_Model $clientes_model
 * @property Corretores_Model $corretores_model
 * @property Proposta_Model $proposta_model
 */
class Email extends Base_Controller
{
	public function __construct()
	{
		parent::__construct();

		header('Access-Control-Allow-Origin: *');

		$this->load->model('simples/imovel_model');
		$this->load->model('simples/proposta_model');
		$this->load->model('simples/corretores_model');
		$this->load->model('simples/notificacao_model');
	}

	public function proposta_visualizar()
	{
		$proposta = $this->monta_proposta_dados($_GET);

		$data['proposta'] = $this->monta_proposta_html($proposta);

		$this->load->view('simples/proposta/visualizar', $data);
	}

	public function proposta_enviar()
	{
		$resposta['status'] = false;

		$proposta = $this->monta_proposta_dados($_POST);

		$corretor = $this->corretores_model->obter_informacoes_basicas($proposta->id_corretor);

		if($this->imovel_model->pelos_codigos($proposta->cod_imoveis) != null)
		{
			$proposta->id = $this->proposta_model->novo($proposta, $_POST['cod_imoveis']);

			if($proposta->id > 0)
			{
				$proposta->cod_imoveis = $_POST['cod_imoveis'];//transforma em array novamente para o email!

				$resposta['status'] = enviar_email(
					$proposta->cliente_email,
					'Proposta para você!',
					$this->monta_proposta_html($proposta),
					$corretor->email,
					(bool)$_POST['copia_remetente']);
			}
		}
		else
			$resposta['msg'] = 'Imóveis informados não foram encontrado. Por favor verifique.';

		echo json_encode($resposta);
	}

	private function monta_proposta_dados($dados)
	{
		$proposta = new Proposta_Model();
		$proposta->id 				= NULL;
		$proposta->assunto 			= $dados['assunto'];
		$proposta->cliente_nome 	= $dados['cliente_nome'];
		$proposta->cliente_email 	= $dados['cliente_email'];

		if(!isset($dados['cod_imoveis']))
			$proposta->cod_imoveis = array();
		else if(is_array($dados['cod_imoveis']))
			$proposta->cod_imoveis = $dados['cod_imoveis'];
		else
			$proposta->cod_imoveis 	= array_map('trim', explode(',', $dados['cod_imoveis']));

		$proposta->id_corretor 	= $dados['id_corretor'];
		$proposta->data 		= date("Y-m-d H:i:s");

		return $proposta;
	}

	private function monta_proposta_html($proposta)
	{
		$imoveis_html = '';

		if(count($proposta->cod_imoveis) > 0)
			$imoveis_html = monta_div_imoveis_para_proposta($this->imovel_model->pelos_codigos($proposta->cod_imoveis), $proposta->id);

		return monta_corpo_email(array(	'nome' 		=> $proposta->cliente_nome,
			'assunto'	=> $proposta->assunto,
			'imoveis' 	=> $imoveis_html),
			'proposta_para_cliente',
			'',
			obter_arquivo_rodape($proposta->id_corretor));
	}

	public function visualizar_resposta()
	{
		if($_GET['notificacao_tipo'] == 'normal')
			echo $this->monta_resposta_normal($_GET['id'], $_GET['assunto'], $_GET['id_corretor']);
		else if($_GET['notificacao_tipo'] == 'interesse_em_imovel')
			echo $this->monta_resposta_interesse_em_imovel($_GET['id'], $_GET['assunto'], $_GET['id_corretor']);
	}

	public function monta_resposta_normal($id, $resposta_corpo, $id_corretor)
	{
		$this->load->model('contato_model');

		$contato = $this->contato_model->obter($id);

		$contato->assunto = $resposta_corpo;

		return $this->monta_resposta_normal_html($this->monta_resposta_dados((array)$contato, $id_corretor));
	}

	/**
	 * Retorna o contato com o TIPO normal
	 * O tipo é o que diferencia as tabelas
	 * Tipos :
	 * 		normal = 'tb_contato',
	 * 		interesse_em_imovel = 'tb_contato_imovel_interesse'
	 */
	private function monta_resposta_interesse_em_imovel($id, $resposta_corpo, $id_corretor)
	{
		$this->load->model('contato_imovel_interesse_model');

		$contato = $this->contato_imovel_interesse_model->obter($id);

		$contato->assunto = $resposta_corpo;

		return $this->monta_resposta_interesse_em_imovel_html($this->monta_resposta_dados((array)$contato, $id_corretor));
	}

	private function monta_resposta_dados($dados, $id_corretor)
	{
		$resposta = new stdClass();
		$resposta->assunto 			= $dados['assunto'];
		$resposta->cliente_nome 	= $dados['nome'];
		$resposta->cliente_email 	= $dados['email'];

		if(isset($dados['cod_imovel']))
			$resposta->cod_imoveis 		= array_map('trim', explode(',', $dados['cod_imovel']));

		$resposta->id_corretor 		= $id_corretor;
		$resposta->data 			= date("Y-m-d H:i:s");

		return $resposta;
	}

	private function monta_resposta_interesse_em_imovel_html($resposta)
	{
		$this->load->model('imovel_model');
		$imoveis_html = monta_div_imoveis_para_proposta($this->imovel_model->pelos_codigos($resposta->cod_imoveis));

		return monta_corpo_email(array(	'nome' 		=> $resposta->cliente_nome,
			'assunto'	=> $resposta->assunto,
			'imoveis' 	=> $imoveis_html),
			'resposta_para_cliente_interessado_em_imovel',
			'',
			obter_arquivo_rodape($resposta->id_corretor));
	}

	private function monta_resposta_normal_html($resposta)
	{
		return monta_corpo_email(array(	'nome' 		=> $resposta->cliente_nome,
			'assunto'	=> $resposta->assunto),
			'resposta_para_cliente_normal',
			'',
			obter_arquivo_rodape($resposta->id_corretor));
	}

	public function enviar_resposta()
	{
		$this->load->model('simples/notificacao_resposta_model');

		/** @var Notificacao_Resposta_Model $resposta */
		$resposta = $this->monta_resposta($_POST['id'], $_POST['notificacao_tipo'], $_POST['assunto'], $this->corretores_model->obter_informacoes_basicas($_POST['id_corretor']));

		if($_POST['notificacao_tipo'] == 'normal')
			$html = $this->monta_resposta_normal($resposta->contato_id, $resposta->resposta, $resposta->id_corretor);
		else if($_POST['notificacao_tipo'] == 'interesse_em_imovel')
			$html = $this->monta_resposta_interesse_em_imovel($resposta->contato_id, $resposta->resposta, $resposta->id_corretor);

		$enviado = enviar_email($resposta->cliente_email, 'Em resposta ao seu contato', $html, $resposta->corretor_email, (bool)$_POST['copia_remetente']);
		$salvo_resposta = $this->notificacao_resposta_model->novo($resposta) > 0;
		$salvo_notificacao = $this->notificacao_model->marcar_como_respondida($resposta->contato_id, $resposta->contato_tipo);

		echo json_encode(array('status' => $enviado && $salvo_resposta && $salvo_notificacao));
	}

	private function monta_resposta($id , $notificacao_tipo, $resposta_corpo, $corretor)
	{
		$this->load->model('notificacao_resposta_model');
		$this->load->model('contato_model');
		$this->load->model('contato_imovel_interesse_model');

		if($notificacao_tipo == 'normal')
			$notificacao = $this->contato_model->obter($id);
		else if($notificacao_tipo == 'interesse_em_imovel')
			$notificacao = $this->contato_imovel_interesse_model->obter($id);

		/** @var Notificacao_Resposta_Model $resposta */
		$resposta = new Notificacao_Resposta_Model();
		$resposta->id_corretor 		= $corretor->id_corretor;
		$resposta->cliente_email 	= $notificacao->email;
		$resposta->corretor_email 	= $corretor->email;
		$resposta->resposta 		= $resposta_corpo;
		$resposta->contato_id 		= $notificacao->id;
		$resposta->contato_tipo 	= $notificacao_tipo;
		$resposta->respondido_em 	= date("Y-m-d H:i:s");

		return $resposta;
	}

	public function obter_rodape_padrao($id_corretor = NULL)
	{
		$arquivo = obter_arquivo_rodape($id_corretor, FALSE);

		header('Content-Type: ' . getimagesize($arquivo)['mime']);
		header('Content-Length: ' . filesize($arquivo));

		fpassthru(fopen($arquivo, 'rb'));
	}

	/**
	 * Testa configuração cadastrada no formulário da imobiliária
	 */
	public function testar()
	{
		$_SESSION['filial']['email_sender_host'] = $_POST['email_sender_host'];
		$_SESSION['filial']['email_sender_porta'] = $_POST['email_sender_porta'];
		$_SESSION['filial']['email_sender'] = $_POST['email_sender'];
		$_SESSION['filial']['email_sender_senha'] = $_POST['email_sender_senha'];

		echo json_encode(array('status' => enviar_email($_POST['email'], 'Email de teste SimplesImob', 'Apenas um email de teste enviado pelo SimplesImob', $_POST['email_sender'])));
	}
}
