<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Simples extends MX_Controller
{
	public function index()
	{
		$config = $this->config->item('admin');

		if( ! isset($config['imobiliaria']['chave']) )
			die('Identificador da imobiliária não configurado.');

		header('location: http://www.simplesimob.com.br/admin?imobiliaria=' . $config['imobiliaria']['chave']);
	}
}
