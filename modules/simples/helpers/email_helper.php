<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('valor_imovel_formater_helper.php');

function enviar_email($para, $assunto, $corpo, $remetente = '', $copia_remetente = false)
{
    $CI =& get_instance();

    $CI->load->library('simples/F_PHPMailer');

    $mail = new PHPMailer();
    //$mail->SMTPDebug  = 1;
    $mail->IsSMTP(); //Definimos que usaremos o protocolo SMTP para envio.
    $mail->SMTPAuth = true; //Habilitamos a autenticação do SMTP. (true ou false)
    $mail->Host = $_SESSION['filial']['email_sender_host']; //smtp
    $mail->Port = $_SESSION['filial']['email_sender_porta']; //Estabelecemos a porta utilizada pelo servidor de email.
    $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
    $mail->Username = $_SESSION['filial']['email_sender']; //Usuário do gMail
    $mail->Password = $_SESSION['filial']['email_sender_senha']; //Senha do gMail

    if($remetente == '')
        $remetente = $_SESSION['filial']['email_padrao'];

    $mail->SetFrom($remetente, '=?UTF-8?B?'.base64_encode($_SESSION['filial']['nome']).'?='); //Quem está enviando o e-mail.
    $mail->AddReplyTo($remetente, '=?UTF-8?B?'.base64_encode($_SESSION['filial']['nome']).'?='); //Para que a resposta será enviada.

    $mail->CharSet = "ISO-8859-1";
    $mail->Subject = '=?UTF-8?B?'.base64_encode($assunto).'?='; //Assunto do e-mail.

    $mail->AddBCC($_SESSION['filial']['email_gerente'], $assunto); // Cópia Oculta
    //$mail->AddBCC('register@singulo.com.br', $assunto); // Cópia Oculta

    if($copia_remetente)
        $mail->AddBCC($remetente, 'Cópia oculta - ' . $assunto);

    $mail->Body = utf8_decode($corpo);
    $mail->AddAddress($para);
    return $mail->Send();
}

function monta_corpo_email(array $trocar_palavras, $arquivo, $filial = '', $assinatura_url = NULL)
{
    $CI =& get_instance();

    $arquivo_caminho_completo = obter_arquivo_html_email($arquivo);//!is_null($CI->config->item($arquivo, 'email_corpos')) ? $CI->config->item($arquivo, 'email_corpos') : $CI->config->item('email')['html'] . $arquivo . '.html';

    $email_padrao = file_get_contents(obter_arquivo_html_email('email_padrao') );

    if($assinatura_url == NULL)
        $assinatura_url = obter_arquivo_rodape();

    if($filial == '')
        $filial = strtolower($_SESSION['filial']['chave']);

    $trocar_palavras['imobiliaria_nome'] = $_SESSION['filial']['nome'];

    $email_config = $CI->config->item('email');

    //SITE LINK
    $trocar_palavras['site_link'] = $_SESSION['filial']['link'];
    //FILIAL NOME
    $trocar_palavras['filial_nome'] = $filial;
    //RODAPE ASSINATURA
    $trocar_palavras['assinatura_url'] = $assinatura_url;
    //CABEÇALHO
    $trocar_palavras['cabecalho_url']  = isset($email_config['cabecalho']) ? $email_config['cabecalho'] : base_url('modules/simples/assets/email/imagens/cabecalho.jpg');
    //INSTAGRAM
    $trocar_palavras['instagram_link'] = $_SESSION['filial']['instagram'];
    $trocar_palavras['instagram_icon_url']  = base_url('modules/simples/assets/email/imagens/icon-instagram.jpg');
    //FACEBOOK
    $trocar_palavras['facebook_link'] = $_SESSION['filial']['facebook'];
    $trocar_palavras['facebook_icon_url']  = base_url('modules/simples/assets/email/imagens/icon-facebook.jpg');

    $corpo = file_get_contents($arquivo_caminho_completo);
    foreach($trocar_palavras as $palavra => $valor)
    {
        $corpo = str_replace('$' . $palavra . '$', $valor, $corpo);

        $email_padrao = str_replace('$' . $palavra . '$', $valor, $email_padrao);
    }

    return str_replace('$corpo$', $corpo, $email_padrao);
}

function monta_div_imoveis_para_proposta($imoveis, $id_proposta = NULL)
{
    $CI =& get_instance();
    $CI->load->helper('text');
    $CI->load->helper('url');

    $email_config = $CI->config->item('email');
    $div = file_get_contents($email_config['html'] . 'imoveis.html');;

    $div_imoveis = '';

    foreach($imoveis as $imovel)
    {
        $div_imovel = substr($div, 0);

        $div_imovel = str_replace('$link$',  $_SESSION['filial']['link'] . 'imovel?id=' . $imovel->id . '&filial=' . strtolower($_SESSION['filial']['chave']) . (is_null($id_proposta) ? '' : '&id_proposta=' . $id_proposta), $div_imovel);
        $div_imovel = str_replace('$src_img$', $_SESSION['filial']['fotos_imoveis'] . $imovel->foto, $div_imovel);
        $div_imovel = str_replace('$valor$', format_valor_miniatura($imovel, 'R$'), $div_imovel);
        $div_imovel = str_replace('$cidade$', $imovel->cidade, $div_imovel);
        $div_imovel = str_replace('$tipo$', $_SESSION['filial']['tipos_imoveis'][$imovel->id_tipo]->tipo, $div_imovel);
        $div_imovel = str_replace('$codigo$', $imovel->id, $div_imovel);
        $div_imovel = str_replace('$imovel_desc$', character_limiter($imovel->descricao, 200), $div_imovel);

        $div_imoveis .= $div_imovel;
    }

    return $div_imoveis;
}

function obter_arquivo_html_email($arquivo)
{
    $CI =& get_instance();

    $filial = $CI->session->userdata('filial');
    $arquivo_em_pasta_filial = RAIZPROJETO . $filial['chave'] . '/email/html/' . $arquivo . '.html';

    if(file_exists($arquivo_em_pasta_filial))
        return $arquivo_em_pasta_filial;
    else
    {
        $email_config = $CI->config->item('email');
        return $email_config['html'] . $arquivo . '.html';
    }
}

function obter_arquivo_rodape($id_corretor = NULL, $externo = TRUE)
{
    $CI =& get_instance();

    $assinatura = '';

    if( ! is_null($id_corretor))
        if($externo)
            $assinatura =  $_SESSION['filial']['fotos_corretores_assinaturas_externo'] . $id_corretor . '.png';
        else
            $assinatura =  $_SESSION['filial']['fotos_corretores_assinaturas_interno'] . $id_corretor . '.png';

    if(isset($_SESSION['filial']['fotos_corretores_assinaturas_interno']) && file_exists($_SESSION['filial']['fotos_corretores_assinaturas_interno'] . $id_corretor . '.png'))
        return $assinatura;
    else
    {
        $email_config = $CI->config->item('email');
        if($externo)
            return isset($email_config['rodape']) ? $email_config['rodape'] : base_url('modules/simples/assets/email/imagens/rodape.jpg');
        else
            return isset($email_config['rodape_interno']) && file_exists($email_config['rodape_interno']) ? $email_config['rodape_interno'] : MODULESPATH . 'simples/assets/email/imagens/rodape.jpg';
    }
}