<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function base_url_filial($acrescentar = '', $setar_filial = true)
{
    if(! $setar_filial)
        return base_url($acrescentar);


    $filial_acrescentar = 'filial=' . $_SESSION['filial']['chave'];

    if(strpos($acrescentar, '?') !== false)
        $filial_acrescentar = $acrescentar . '&' . $filial_acrescentar;
    else
        $filial_acrescentar = $acrescentar . '?' . $filial_acrescentar;

    return base_url($filial_acrescentar);
}
