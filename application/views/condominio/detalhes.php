<? $this->load->view('templates/header'); ?>
<!--  DETALHE -->
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/imovel/detalhe.css'); ?>">
<? $this->load->view('templates/menu'); ?>
<? $this->load->view('templates/banner-logo-elemento', array('elemento' => '<img class="img-responsive" src="' . base_url('assets/images/banner-detalhes-condominio.jpg') . '">')); ?>
<? $this->load->view('templates/filtro'); ?>

<div class="col-xs-12 container-conteudo">
    <div class="row imovel-detalhes">
        <div class="col-md-6" style="margin-left: 7.333333%">
            <div class="col-xs-12">
                <div id="slider-condominio" class="flexslider">
                    <ul class="slides">
                        <? foreach ($condominio->fotos['normais'] as $foto) : ?>
                            <li>
                                <img class="img-responsive" src="<?= $_SESSION['filial']['fotos_condominios'] . $foto->arquivo; ?>" onerror="this.src='<?= base_url('assets/images/imovel-sem-foto.jpg');?>'" />
                            </li>
                        <? endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12">
                <h3>Detalhes</h3>
                <p><?= ucfirst(strtolower($condominio->descricao)); ?></p>
            </div>
        </div>
        <div class="col-md-5">
            <h2 class="text-uppercase"><?= $condominio->nome; ?></h2>
            <p><?= $condominio->cidade; ?> <br>Cód. <?= $condominio->id; ?></p>
            <? if(count($condominio->complementos) > 0) : ?>
                <h3>Complementos</h3>
                <div class="row">
                    <? foreach( $condominio->complementos as $complemento) : ?>
                        <div class="col-md-5">
                            <p><span class="glyphicon glyphicon-ok-sign"> </span> <?= trim($complemento->complemento); ?></p>
                        </div>
                    <? endforeach; ?>
                </div>
            <? endif; ?>
        </div>
    </div>
</div>

<? if(count($condominio->imoveis) > 0) : ?>
    <div class="col-xs-12">
        <div class="row imoveis-condominio">

            <div class="container">
                <div class="col-md-6">
                    <div class="text-left"><h2><em>Imóveis em <strong><?= $condominio->nome; ?></strong></em></h2></div>
                </div>
                <div class="col-md-6" style="margin-top: 20px">
                    <hr>
                </div>
            </div>

            <div class="container">
                <? require_once(APPPATH . '../modules/simples/helpers/valor_imovel_formater_helper.php'); ?>
                <? require_once(APPPATH . '../modules/simples/helpers/texto_helper.php'); ?>

                <? foreach ($condominio->imoveis as $imovel) : ?>
                    <a href="<?= base_url('imovel?id=' . $imovel->id); ?>">
                        <div class="imovel col-md-6">
                            <div class="col-md-6">
                                <img class="img-responsive" src="<?= $_SESSION['filial']['fotos_imoveis'] . $imovel->foto; ?>" onerror="this.src='<?= base_url('assets/images/imovel-sem-foto.jpg');?>'">
                            </div>
                            <div class="col-md-6 detalhes-imovel">
                                <div class="text-left">
                                    <h3><?= $_SESSION['filial']['tipos_imoveis'][$imovel->id_tipo]->tipo; ?> <? if($imovel->dormitorios > 0) echo ' ' . $imovel->dormitorios . ' ' . texto_para_plural_se_necessario($imovel->dormitorios, 'dormitório'); ?></h3>
                                    <p class="imovel-cidade-codigo"><?= $imovel->cidade; ?> - Cód. <?= $imovel->id; ?></p>
                                </div>
                                <p class="imovel-descricao"><?= $out = strlen($imovel->descricao) > 205 ? substr($imovel->descricao,0,202)."..." : $imovel->descricao; ?></p>
                                <b><small>Valor do imóvel</small></b>
                                <h3 class="no-margin-top"><small><b>R$</b></small> <?= format_valor($imovel->valor); ?></h3>
                            </div>
                        </div>
                    </a>
                <? endforeach; ?>
            </div>
        </div>
    </div>
<? endif; ?>

<? $this->load->view('templates/menu-rodape'); ?>
<? $this->load->view('templates/footer'); ?>

<link rel="stylesheet" type="text/css" href="<?= base_url('assets/flexslider/flexslider.css'); ?>">
<script type="text/jscript" src="<?= base_url('assets/flexslider/jquery.flexslider-min.js'); ?>" ></script>

<script>
    $(window).load(function() {
        $('#slider-condominio').flexslider({
            animation: "slide",
            controlNav: false
        });
    });
</script>