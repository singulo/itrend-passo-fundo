<? $this->load->view('templates/header'); ?>
    <!--  CONDOMINIOS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/imovel/condominios-lancamentos.css'); ?>">
<? $this->load->view('templates/menu'); ?>
<? $this->load->view('templates/banner-logo-elemento', array('elemento' => '<img class="img-responsive" src="' . base_url('assets/images/banner-condominios.jpg') . '">')); ?>
<? $this->load->view('templates/filtro'); ?>

<div class="container container-conteudo">
    <h1>CONDOMÍNIOS</h1>
    <p>A seguir os principais condomínios do litoral norte, com toda a infraestrutura que você merece para desfrutar os momentoa mais felizes da sua vida.</p>
        <? foreach ($condominios as $condominio) : ?>
            <div class="col-md-3 no-padding-left text-center">
                <div class="imovel">
                    <a href="<?= base_url_filial('condominio?id=' . $condominio->id); ?>" title="Ir para <?= $condominio->nome; ?>">
                        <div class="imovel-img " style="background-image: url(<?= $_SESSION['filial']['fotos_condominios'] . $condominio->foto; ?>), url(<?=base_url_filial('assets/images/imovel-sem-foto.jpg',false); ?>);"></div>
                        <div class="nome-condominio">
                            <h3><?= $condominio->nome; ?></h3>
                        </div>
                    </a>
                </div>
            </div>
        <? endforeach; ?>
</div>

<? $this->load->view('templates/menu-rodape'); ?>
<? $this->load->view('templates/footer'); ?>