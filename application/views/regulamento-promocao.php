<? $this->load->view('templates/header'); ?>
    <!--  DETALHE -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/imovel/detalhe.css'); ?>">
<? $this->load->view('templates/menu'); ?>
<? $this->load->view('templates/banner-logo-elemento', array('elemento' => '<img class="img-responsive" src="' . base_url('assets/images/banner-regulamento.jpg') . '">')); ?>
<? $this->load->view('templates/filtro'); ?>


<div class="col-xs-12 container-conteudo">
    <div class=" container regulamento">
        <h2>REGULAMENTO PROMOÇÃO ITREND IPHONE 7</h2>
        REALIZADORA:

        <img class="pull-right" src="<?= base_url('assets/images/iphones7.jpg'); ?>" title="Imagem meramente ilustrativa" style="width: 330px; margin-left: 50px; margin-bottom: 50px;">

        <em>ITREND IMÓVEIS</em>, pessoa jurídica de direito privado inscrita no CNPJ nº. 23.018.637/001-00 com sede na Av. Ubirajara, 427 Lj.05; Centro - Capão da Canoa - RS, exerce a atividade econômica de intermediação remunerada entre fornecedores e consumidores de serviços imobiliários.

        A aplicabilidade desse Regulamento se efetivará a partir da aquisição da(s) unidade(s) imobiliária(s) integrante(s) desta Promoção, que somente poderá ocorrer mediante a leitura, compreensão e concordância com a integralidade de seus termos.

        <ul>
            <li>1. CONSIDERAÇÕES INICIAIS</li>
            <li>1.1. Esta promoção é de iniciativa da REALIZADORA, que acontecerá no Estado do Rio Grande do Sul, destinada a todo e qualquer consumidor, pessoa física ou jurídica, residente em território nacional, que preencha as condições de participação deste regulamento.</li>
            <li>1.2. Não serão objeto desta promoção, tampouco objeto de distribuição gratuita, quaisquer dos produtos vedados pelo Dec. 70.951/72, tais como: medicamentos, armas, munições, explosivos, fogos de artifício, bebidas alcoólicas, fumos e derivados.</li>
            <li>1.3. A promoção é dispensável de registro, nos termos da Lei 5.768/71, pelo Decreto 70.951/72 e pela Portaria SEAE/MF 184/2006, por não assemelhar-se à nenhuma modalidade de concurso, vale brinde, sorteio ou modalidade assemelhada.</li>
        </ul>

        <ul>
            <li>2. DO OBJETO DA PROMOÇÃO E DOS EMPREENDIMENTOS PARTICIPANTES</li>
            <li>O objeto da Promoção consiste na entrega de um “IPHONE 7” como prêmio, após o fechamento do contrato de compra e venda da unidade imobiliária assinado, com pagamento do sinal e recebimento da comissão em decorrência de assessoria imobiliária ITREND, desde que a unidade seja efetivamente adquirida pelo cliente nos termos deste Regulamento. Válido na aquisição de imóveis no valor igual ou superior a 400.000 reais.</li>
        </ul>

        <ul>
            <li>3. DO PERÍODO DE DURAÇÃO DA PROMOÇÃO</li>
            <li>3.1. Esta promoção terá início no dia 07/10/2016 e término no dia 01/05/2017.</li>
            <li>3.2. A exclusivo critério da REALIZADORA, esta promoção poderá, a qualquer tempo, ser prorrogada e/ou suspensa, ou, ainda, cancelada sem prejuízo dos participantes que já tiverem sido contemplados.</li>
            <li>3.3. A Promoção abrange exclusivamente as unidades dos empreendimentos relacionados neste Regulamento e que obrigatoriamente venham a ser adquiridas no período da promoção.</li>
            <li>3.3.1. Não serão beneficiadas da presente Promoção as unidades que estejam em negociação durante o período da promoção e sejam firmadas (assinatura do contrato de compra e venda com o pagamento -integral- da primeira parcela -sinal- ) fora do período de duração da Promoção.</li>
        </ul>

        <ul>
            <li>4. COMO PARTICIPAR</li>
            <li>4.1. O cliente que firmar o contrato de aquisição proveniente de uma unidade dos empreendimentos participantes proveniente da assessoria imobiliária de um dos corretores da REALIZADORA ITREND IMÓVEIS e efetuar o pagamento integral da primeira parcela (sinal) prevista no contrato será denominado, para fins desta promoção, de CLIENTE PARTICIPANTE.</li>
            <li>4.2. Caso o pagamento da primeira parcela (sinal) não seja efetuado em espécie (dinheiro) a participação somente será computada e considerada válida, para fins desta promoção, após a comprovação e confirmação do depósito ou a compensação do(s) cheque(s), conforme o caso.</li>
            <li>4.3. Será desconsiderado para fins dessa promoção o CLIENTE PARTICIPANTE que praticar ato ilegal, ilícito ou que contrariar os objetivos e regras deste regulamento.</li>
            <li>4.4 Será somente considerado para fins dessa promoção o CLIENTE que firmar contrato de aquisição tendo como ÚNICA e EXCLUSIVA intermediador um dos corretores da REALIZADORA – ITREND IMÓVEIS.</li>
        </ul>

        <ul>
            <li>5. DO IMPEDIMENTO DE PARTICIPAÇÃO E DA DESCLASSIFICAÇÃO</li>
            <li>5.1. Estão impedidos de participar desta promoção os funcionários das REALIZADORAS, assim como de suas sócias; os corretores de imóveis; os clientes que tiverem o Contrato de Compra e Venda da Unidade Imobiliária dos Empreendimentos Participantes firmado com a REALIZADORA rescindido ou distratado; e os clientes que não preencham, por qualquer motivo, os requisitos estipulados neste regulamento.</li>
            <li>5.2. O CLIENTE PARTICIPANTE será, ainda, excluído automaticamente da promoção, em caso de desclassificação e/ou fraude comprovada.</li>
            <li>5.3. Caso haja fraude comprovada qualquer das, a seu critério, ajuizar ação cabível contra o CLIENTE PARTICIPANTE que a esta der causa.</li>
            <li>5.4. Havendo desistência, distrato ou rescisão do contrato firmado em relação aos Empreendimentos Participantes por qualquer motivo, este ficará, automaticamente, desclassificado, não sendo devida qualquer premiação pelas REALIZADORAS, desde o momento da aquisição, se ocorrer, até o momento da utilização do prêmio.</li>
        </ul>

        <ul>
            <li>6. DA APURAÇÃO</li>
            <li>6.1. Os dados apresentados pelo CLIENTE PARTICIPANTE serão verificados por preposto da REALIZADORA, bem como o preenchimento de todos os requisitos necessários para a participação válida nesta promoção.</li>
        </ul>

        <ul>
            <li>7. PREMIAÇÃO:</li>
            <li>7.1. Consiste o prêmio objeto desta Promoção, em um “IPHONE 7”, oferecido pela REALIZADORA e que será ENTREGUE após assinado em decorrência de assessoria imobiliária da REALIZADORA ITREND IMÓVEIS.</li>
            <li>7.2. O cliente que adquirir uma unidade do(s) Empreendimento(s) Participante(s) da promoção observada as disposições contidas no presente Regulamento, fará jus ao prêmio previsto nesta cláusula.</li>
            <li>7.2.1. Somente será considerado adquirente da unidade o cliente que firmar o contrato de aquisição de uma unidade dos empreendimentos participantes proveniente da assessoria imobiliária de um dos corretores da REALIZADORA ITREND IMÓVEIS, efetuar o pagamento integral da parcela (sinal) prevista no contrato, independentemente do fluxo financeiro adotado pelo cliente, e não estiver por qualquer das formas previstas neste Regulamento ou na legislação vigente impedido de participar.</li>
            <li>7.4. Em sendo o cliente adquirente de mais de uma unidade residencial, este fará jus a um prêmio para cada unidade adquirida, exclusivamente se pertencentes aos Empreendimentos Participantes desta promoção na forma do Regulamento.</li>
        </ul>

        <ul>
            <li>8. ENTREGA E DA UTILIZAÇÃO DO(S) PRÊMIO(S)</li>
            <li>8.1. O(s) prêmio(s) será(ão) disponibilizado(s) para os ganhadores no prazo de 10 DIAS APÓS O LANÇAMENTO DO IPHONE 7 NO BRASIL e 30 (trinta) dias contados da assinatura do “Contrato Particular de Promessa de Compra e Venda” ou do pagamento (integral) da primeira parcela (sinal) prevista no contrato, o que ocorrer por último, desde que preenchidos todos os requisitos deste Regulamento. No ato da disponibilização do(s) prêmio(s), os ganhador(es) deverá(ão) assinar um TERMO DE QUITAÇÃO E ENTREGA DE PRÊMIO.</li>
            <li>8.2. O(s) prêmio(s) não poderá(ão) ser convertido(s) em dinheiro, em desconto/crédito para abatimento no valor da unidade imobiliária adquirida, tampouco ser trocado(s) por qualquer outro produto.</li>
            <li>8.3. O(s) prêmio(s) disponibilizado(s) pode(m) ser transferido(s) pelo CLIENTE PARTICIPANTE por sua total responsabilidade e risco.</li>
            <li>8.4. O prêmio disponibilizado através da presente Promoção poderá ser utilizado pelo CLIENTE PARTICIPANTE, desde que preenchidos todos os requisitos deste Regulamento, em até 6(seis) meses, contados da assinatura do “Contrato Particular de Promessa de Compra e Venda” ou do pagamento (integral) da primeira parcela (sinal) prevista no contrato, o que ocorrer por último. Após esse prazo o CLIENTE PARTICIPANTE perderá qualquer direito referente a Premiação antes concedida, não lhe restando nenhum bônus a ser recebido.</li>
        </ul>

        <ul>
            <li>9. DIVULGAÇÃO DA PROMOÇÃO</li>
            <li>9.1. A divulgação desta promoção poderá ser feita por quaisquer meios de comunicação, que serão escolhidos exclusivamente pela REALIZADORA, como, mas não se limitando a utilização de material em stand de vendas, internet, podendo ser realizada, ainda, por meio de anúncios e visitas as imobiliárias.</li>
            <li>11. DISPOSIÇÕES GERAIS</li>
            <li>11.1. A promoção regulamentada neste documento poderá ser alterada ou cancelada por conveniência das REALIZADORAS, a qualquer momento, independente de prévia comunicação/notificação, sem que importe em nenhum tipo de indenização, cabendo somente ao(s) participante(s), a responsabilidade por manter-se informado sobre o sistema e a validade da promoção.</li>
            <li>11.2. O(s) participante(s) autoriza(m), desde já, em caráter irretratável e irrevogável, a utilização, pelas REALIZADORAS, bem como por suas sócias, de seus nomes, imagem, som de voz, endereços físicos, eletrônicos, telefones e demais dados constantes no formulário, em qualquer um dos meios escolhidos pelas mesmas, pelo período de 01 (um) ano, a contar da data da apuração, com o propósito de reforço de mídia publicitária e divulgação do evento em referência, bem como, para comunicação de futuras campanhas, sem nenhum ônus para a empresas.</li>
            <li>11.3. Excluem-se da participação na presente promoção, os menores de 18(dezoito) anos, os sócios e funcionários das empresas REALIZADORAS, assim como por qualquer sócio e funcionário de sociedade coligada ou participada da mesma.</li>
            <li>11.4. As condições desta promoção não são cumulativas com outros programas ou promoções já efetuadas pela REALIZADORA ou sociedade coligada ou participada da mesma.</li>
            <li>11.5. As eventuais dúvidas, divergências ou situações não previstas neste Regulamento oriundas dos participantes da promoção serão dirimidas pela REALIZADORA.</li>
            <li>11.6. A responsabilidade da REALIZADORA dar-se-á por encerrada no ato de disponibilização da premiação ao ganhador.</li>
            <li>11.7. A qualquer instante, a REALIZADORA poderá solicitar documentos comprobatórios dos participantes, averiguando a veracidade das informações prestadas neste concurso, desclassificando sumariamente aqueles que prestarem quaisquer informações falsas, não cabendo aos mesmos qualquer recurso contra referidas decisões, podendo ainda a REALIZADORA ou empresas parceiras adotar contra os mesmos todas as medidas judiciais cíveis e criminais pertinentes.</li>
            <li>11.8. A simples participação nesta promoção caracteriza a aceitação de todos os termos e condições previstos nesse Regulamento, sem qualquer ressalva ou restrição.</li>
        </ul>

        REALIZADORA:
        <br>
        <em>ITREND IMÓVEIS</em>
    </div>
</div>

<? $this->load->view('templates/menu-rodape'); ?>
<? $this->load->view('templates/footer'); ?>

<style>
    .regulamento ul li:first-child
    {
        text-transform: uppercase;
        font-weight: 600;
    }
</style>
