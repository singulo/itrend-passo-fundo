<? require_once MODULESPATH . 'simples/helpers/texto_helper.php'; ?>

<? $this->load->view('templates/header', ['appendTags' => [
    '<meta property="og:title" content="' . $_SESSION['filial']['tipos_imoveis'][$imovel->id_tipo]->tipo . ' ' . ($imovel->dormitorios > 0 ? $imovel->dormitorios . ' ' . texto_para_plural_se_necessario($imovel->dormitorios, 'dormitório') : '') . ' - ' . $_SESSION['filial']['nome'] . '">',
    '<meta property="og:site_name" content="' . $_SESSION['filial']['nome'] . '">',
    '<meta property="og:image" content="' . $_SESSION['filial']['fotos_imoveis'] . $imovel->fotos_principais[0]->arquivo . '">',
    '<meta property="og:description" content="' . substr($imovel->descricao,0,152) . '...' . '">'
]]); ?>
<!--  DETALHE -->
<link rel="stylesheet" type="text/css" href="<?= base_url_filial('assets/css/imovel/detalhe.css', false); ?>">
<? $this->load->view('templates/menu'); ?>
<? $this->load->view('templates/banner-logo-elemento', array('elemento' => '<img class="img-responsive" src="' . base_url_filial('assets/images/banner-detalhes-imovel.jpg') . '">')); ?>
<? $this->load->view('templates/filtro'); ?>

<? $this->load->view('imovel/contato-interesse'); ?>

<? require_once(APPPATH . '../modules/simples/helpers/valor_imovel_formater_helper.php'); ?>
<? require_once(APPPATH . '../modules/simples/helpers/texto_helper.php'); ?>

<div class="col-xs-12 container-conteudo">
    <div class="container imovel-detalhes">
        <div class="col-md-6">
            <div class="col-xs-12 imovel-img-destaque" style="margin-bottom: 15px">
                <div class="col-md-12 img-principal-imovel" style="background-image: url(<?= $_SESSION['filial']['fotos_imoveis'] . $imovel->fotos_principais[0]->arquivo; ?>), url(<?=base_url_filial('assets/images/imovel-sem-foto.jpg',false); ?>);"></div>
            </div>
            <div class="col-md-12">
                <? if($this->session->has_userdata('usuario')) : ?>
                    <div class="fotos-imovel row">
                        <? foreach($imovel->fotos['normais'] as $foto) : ?>
                            <div class="col-xs-3">
                                <a href="<?= $_SESSION['filial']['fotos_imoveis'] . $foto->arquivo; ?>" rel="imovel-gallery">
                                    <div class="col-md-12 img-imovel" style="background-image: url(<?= $_SESSION['filial']['fotos_imoveis'] . $foto->arquivo;?>), url(<?=base_url_filial('assets/images/imovel-sem-foto.jpg',false); ?>);"></div>
                                </a>
                            </div>
                        <?endforeach; ?>
                    </div>
                <?else :?>
                    <div class="aviso-para-ver-fotos row">
                        <h4>Este imóvel contem <strong><?= $total_fotos; ?> fotos</strong></h4>
                        <p>Tenha acesso as fotos de <strong>todos imóveis</strong>, apenas faça seu cadastro.</p>
                        <button class="btn btn-danger" data-toggle="modal" data-target="#modal-login">Entre/Cadastre-se</button>
                    </div>
                <?endif; ?>
            </div>
        </div>
        <div class="col-md-6 pull-right">
            <div class="pull-left">
                <div class="row">
                    <div class="col-xs-12 col-sm-8">
                        <? if ($_SESSION['filial']['chave'] == $this->config->item('filiais')['itrend-passo-fundo']['chave']): ?>
                            <h2 class="text-uppercase no-margin-top"><b><?= $_SESSION['filial']['tipos_imoveis'][$imovel->id_tipo]->tipo; ?><? if(isset($imovel->condominio)) echo ' | ' . $imovel->condominio->nome; ?></b></h2>
                        <?else :?>
                            <h2 class="text-uppercase no-margin-top"><b><?= $_SESSION['filial']['tipos_imoveis'][$imovel->id_tipo]->tipo; ?><? if($imovel->dormitorios > 0) echo ' | ' . $imovel->dormitorios . ' ' . texto_para_plural_se_necessario($imovel->dormitorios, 'dormitório'); ?></b></h2>
                        <?endif; ?>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <input id="cod_imovel" type="hidden" value="<?= $imovel->id; ?>">
                        <h5 class="text-right">Cód. <?= $imovel->id; ?></h5>
                    </div>
                </div>
                <p class="text-justify descricao-imovel"><?= $imovel->descricao; ?></p>

                <div class="col-md-4 info-icones">
                    <div class="row">
                        <? if($imovel->dormitorios > 0) : ?>
                            <div class="col-xs-4 col-md-12">
                                <div class="col-md-5">
                                    <img class="center-block img-responsive" src="<?= base_url_filial('assets/images/icon-bed.png'); ?>">
                                </div>
                                <div class="col-md-7 no-padding-xs text-center-xs">
                                    <b><?= $imovel->dormitorios;?> dorm.</b>
                                </div>
                            </div>
                        <? endif; ?>
                        <? if($imovel->suites > 0) : ?>
                            <div class="col-xs-4 col-md-12">
                                <div class="col-md-5">
                                    <img class="center-block img-responsive" src="<?= base_url_filial('assets/images/icon-suite.png'); ?>">
                                </div>
                                <div class="col-md-7 no-padding-xs text-center-xs">
                                    <b><?= $imovel->suites;?> suítes</b>
                                </div>
                            </div>
                        <? endif; ?>
                        <? if($imovel->area_total > 0) : ?>
                            <div class="col-xs-4 col-md-12">
                                <div class="col-md-5">
                                    <img class="center-block img-responsive" src="<?= base_url_filial('assets/images/icon-shape-size.png'); ?>">
                                </div>
                                <div class="col-md-7 no-padding-xs text-center-xs">
                                    <span><b><?= $imovel->area_total;?>m²</b></span>
                                </div>
                            </div>
                        <? endif; ?>
                        <? if($imovel->garagem > 0) : ?>
                            <div class="col-xs-4 col-md-12">
                                <div class="col-md-5">
                                    <img class="center-block img-responsive" src="<?= base_url_filial('assets/images/icon-car.png'); ?>">
                                </div>
                                <div class="col-md-7 no-padding-xs text-center-xs">
                                    <b><?= $imovel->garagem;?> vagas</b>
                                </div>
                            </div>
                        <? endif; ?>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="row text-left">
                        <b class="pull-left"><small>Valor do imóvel</small></b>
                        <br>
                        <h2 style="color: #3D3D3D;" class="no-margin-top pull-left"><b><small><b>R$</b></small> <?= format_valor_miniatura($imovel); ?></b></h2><button class="btn btn-danger btn-interesse pull-right" style="margin-bottom: 20px;">SOLICITE CONTATO</button>
                    </div>
                </div>

                <? if( count($imovel->complementos) > 0) : ?>
                    <div class="col-md-8">
                        <div class="row">
                            <h4>Complementos do imóvel</h4>
                            <div class="complementos-imovel">
                                <? foreach($imovel->complementos as $complemento) : ?>
                                    <div class="col-xs-6">
                                        <p><span class="glyphicon glyphicon-ok-sign"> </span> <?= trim($complemento->complemento); ?></p>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        </div>
                    </div>
                <? endif; ?>

                <div class="col-md-5 complementos-imovel">
                    <h4>Localização</h4>
                    <? if(isset($imovel->condominio)) : ?>
                        <a href="<?= (isset($imovel->condominio) ? base_url_filial('condominio?id=' . $imovel->condominio->id) : '#'); ?>" title="Ir para <?= $imovel->condominio->nome; ?>">
                            <? if(isset($imovel->condominio->fotos) && isset($imovel->condominio->fotos[0])) : ?>
                                <img src="<?= $_SESSION['filial']['fotos_condominios'] . $imovel->condominio->fotos[0]->arquivo; ?>" class="img-responsive">
                                <h5 style="margin-bottom: 4px;"><b><?= $imovel->condominio->nome;?></b></h5>
                            <? endif; ?>
                        </a>
                    <? endif; ?>
                    <p>Cidade: <?= $imovel->cidade; ?></p>
                    <p>Bairro: <?= $imovel->bairro; ?></p>

                    <? if(strlen($imovel->lat_lng) > 0) : ?>
                        <div id="map-canvas" data-lat-lng="<?= $imovel->lat_lng;?>" data-center="<?= $imovel->lat_lng;?>" data-circle-around="true" data-marker-none="false" data-radius-size="200" data-map-zoom="15" class="row" style="height: 150px;"></div>
                    <?  endif; ?>
                </div>
                <? if(isset($imovel->condominio) && $imovel->condominio->complementos > 0) : ?>
                    <div class="col-md-7 complementos-empreendimento">
                        <h4>Comp. do empreendimento</h4>
                        <? foreach( $imovel->condominio->complementos as $complemento) : ?>
                            <div class="col-xs-6">
                                <p><span class="glyphicon glyphicon-ok-sign"> </span> <?= $complemento->complemento; ?></p>
                            </div>
                        <? endforeach; ?>
                    </div>
                <? endif; ?>
            </div>
        </div>
    </div>
</div>

<div class="col-xs-12">
    <div class="row imoveis-sugestao">
        <div class="text-center"><h3><em>Confira outros <b>imóveis similares</b></em></h3></div>
        <div class="col-xs-12 col-md-offset-1 col-md-10">
            <div id="slider-imoveis-sugestoes" class="flexslider">
                <ul class="slides">
                    <? foreach($imoveis_similares as $imovel) : ?>
                        <li>
                            <div class="col-xs-12">
                                <div class="col-xs-12 col-md-6 row">
                                    <a href="<?= base_url_filial('imovel?id=' . $imovel->id); ?>">
                                        <div class="col-md-12 img-imoveis-similares" style="background-image: url(<?= $_SESSION['filial']['fotos_imoveis'] . $imovel->foto; ?>), url(<?=base_url_filial('assets/images/imovel-sem-foto.jpg',false); ?>);"></div>

                                    </a>
                                </div>
                                <div class="hidden-xs hidden-sm col-md-6">
                                    <h5 class="text-uppercase"><?= $_SESSION['filial']['tipos_imoveis'][$imovel->id_tipo]->tipo; ?><? if($imovel->dormitorios > 0) echo ' | ' . $imovel->dormitorios . ' ' . texto_para_plural_se_necessario($imovel->dormitorios, 'dormitório'); ?></h5>
                                    <small>Cód. <?= $imovel->id; ?></small>
                                    <div class="linha-horizontal-sugestao"></div>
                                    <p style="font-size: 9px;"><?= $out = strlen($imovel->descricao) > 205 ? substr($imovel->descricao,0,202)."..." : $imovel->descricao; ?></p>
                                    <small><em class="pull-left">Valor do imóvel</em></small>
                                    <h3 class="no-margin-top text-left"><b><br><small><b>R$</b></small> <?= format_valor($imovel->valor); ?></b></h3>
                                </div>
                            </div>
                        </li>
                    <? endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<? $this->load->view('templates/menu-rodape'); ?>
<? $this->load->view('templates/footer'); ?>

<link rel="stylesheet" type="text/css" href="<?= base_url_filial('assets/flexslider/flexslider.css', false); ?>">
<script type="text/jscript" src="<?= base_url_filial('assets/flexslider/jquery.flexslider-min.js', false); ?>" ></script>
<script type="text/jscript" src="<?= base_url_filial('assets/js/detalhe.js', false); ?>" ></script>

<!--MAPA-->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<script src="<?= base_url_filial('assets/js/gmap.js', false); ?>"></script>

<script>
    $(window).load(function() {
        $('#slider-imoveis-sugestoes').flexslider({
            animation: "slide",
            animationLoop: true,
            itemWidth: 100,
            itemMargin: 5,
            maxItems: 2,
            minItems: 1,
            controlNav: false
        });

        $(".fotos-imovel a").fancybox({
            'transitionIn'	:	'elastic',
            'transitionOut'	:	'elastic',
            'speedIn'		:	1600,
            'speedOut'		:	1200,
            'overlayShow'	:	false,
            'loop'	        :	false
        });
    });


</script>

<style>
    .fotos-imovel div
    {
        height: 80px;
        overflow: hidden;
        margin-bottom: 5px;
        margin-top: 10px;
    }

    .fotos-imovel div a
    {

    }

    .fotos-imovel div a img
    {

    }
</style>