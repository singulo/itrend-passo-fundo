<? if( ! isset($filtro))
{
    $filtro = new stdClass();
    $filtro->preco_min = 0;
    $filtro->id = "";
    $filtro->finalidade = array();
    $filtro->preco_max = 3000000;
    $filtro->id_tipo = array();
    $filtro->id_condominio = array();
    $filtro->cidade = array();
    $filtro->dormitorios = NULL;
    $filtro->garagem = NULL;
    $filtro->banheiros = NULL;
    $filtro->suites = NULL;
}
?>

<? $this->load->view('templates/header'); ?>

<!--  PESQUISA -->
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/imovel/pesquisa.css'); ?>">

<? $this->load->view('templates/menu'); ?>
<? $this->load->view('templates/banner-logo-elemento', array('elemento' => '<img class="img-responsive" src="' . base_url('assets/images/banner-pesquisa.jpg') . '">')); ?>

<? require_once MODULESPATH . 'simples/helpers/form_values_helper.php'; ?>

<div class="container">
    <div class="qnt-resultados col-xs-12 col-md-12">
        <h3><em id="msg-resultados-encontrados" class="pull-right"></em></h3>
    </div>
    <div class="col-filtro col-md-3">
        <h3>CONTE-NOS MAIS<br><em>SOBRE SEU SONHO!</em></h3>
        <form class="filtro-imovel" id="filtro-imovel">
            <select class="selectpicker form-control" name="finalidade" data-live-search="true" title="Finalidade" multiple >
                <? foreach( Finalidades::getConstants() as $finalidade => $valor ) : ?>
                    <? if($finalidade != 'Temporada') :?>
                        <option value="<?= $valor;?>" <? if(select_value($valor, $filtro->finalidade)) echo 'selected'; ?>><?= $finalidade; ?></option>
                    <?endif; ?>
                <? endforeach; ?>
            </select>
            <select class="selectpicker form-control" name="id_tipo" data-live-search="true" title="Tipo" multiple data-selected-text-format="count > 1"  data-count-selected-text="{0} tipos">
                <? foreach($_SESSION['filial']['tipos_imoveis'] as $tipo) : ?>
                    <option value="<?= $tipo->id; ?>" <? if(select_value($tipo->id, $filtro->id_tipo)) echo 'selected'; ?>><?= $tipo->tipo;?></option>
                <? endforeach; ?>
            </select>
            <select class="selectpicker form-control" name="dormitorios" title="Dorm">
                <option value="">-</option>
                <? for ($i = 1; $i <= $this->config->item('pesquisa')['suites']; $i++) : ?>
                    <option value="<?= $i; ?>" <? if(is_numeric($filtro->dormitorios) && $i == $filtro->dormitorios) echo 'selected'; ?>><?= $i; ?></option>
                <? endfor; ?>
            </select>

            <select class="selectpicker form-control" name="garagem"  title="Vagas">
                <option value="">-</option>
                <? for ($i = 1; $i <= $this->config->item('pesquisa')['vagas']; $i++) : ?>
                    <option value="<?= $i; ?>" <? if(is_numeric($filtro->garagem) && $i == $filtro->garagem) echo 'selected'; ?>><?= $i; ?></option>
                <? endfor; ?>
            </select>

            <select class="selectpicker form-control" name="suites"  title="Suítes">
                <option value="">-</option>
                <? for ($i = 1; $i <= $this->config->item('pesquisa')['suites']; $i++) : ?>
                    <option value="<?= $i; ?>" <? if(is_numeric($filtro->suites) && $i == $filtro->suites) echo 'selected'; ?>><?= $i; ?></option>
                <? endfor; ?>
            </select>

            <input type="text" name="id" class="form-control" placeholder="CÓDIGO">

            <? $filtro_valor_min = array('valor' => $filtro->preco_min, 'formatado' => number_format((int)$filtro->preco_min, 2, ',', '.')); ?>
            <? $filtro_valor_max = array('valor' => $filtro->preco_max, 'formatado' => number_format((int)$filtro->preco_max, 2, ',', '.')); ?>
            <input type="hidden" name="preco_min" value="<?= $filtro->preco_min;?>">
            <input type="hidden" name="preco_max" value="<?= $filtro->preco_max;?>">
            <div class="filtro-valores">
                <span class="pull-left"><small>R$</small> <?= $filtro_valor_min['formatado']; ?> a</span>

                <!--                                <span class="pull-left">Minimo</span>-->
                <!--                                <span class="pull-right">Máximo </span>-->
                <span class="pull-right"><small>R$</small><?= $filtro_valor_max['formatado']; ?></span>
            </div>
            <input type="text" class="slider-filtro span2" data-slider-tooltip="hide" data-slider-handle="custom"  data-slider-min="0" data-slider-max="3000000" data-slider-step="50000" data-slider-value="[<?= $filtro_valor_min['valor']; ?>, <?= $filtro_valor_max['valor']; ?>]"/>

<!--            <input type="text" class="slider-filtro span2" value="" data-slider-tooltip="hide" data-slider-handle="custom" data-slider-min="0" data-slider-max="3000000" data-slider-step="50000" data-slider-value="[--><?//= $filtro->preco_min;?><!--,--><?//= $filtro->preco_max;?><!--]" data-slider-ticks-labels='["R$0", "R$3.000.000"]'/>-->

            <button type="button" class="btn-pesquisar" onclick="pesquisar(0);">PESQUISAR</button>
        </form>
    </div>

    <div class="col-md-9 imoveis-resultado-pesquisa">
        <div class="col-md-6" id="imovel-visualizacao" style="display: none;">
            <a href="#" class="imovel-url">
                <div class="imovel">
                    <div class="text-left"><h3 class="imovel-tipo">Apartamento 3 dorm.</h3>
                        <span class="imovel-cidade">Atlântida</span><span class="imovel-codigo pull-right">Cód. AA0000</span>
                    </div>
                    <div class="imovel-img col-xs-12"></div>
                    <div class="col-xs-12 text-right">
                        <b><small>Valor do imóvel</small></b>
                        <h2 class="no-margin-top imovel-valor"><small><b>R$</b></small> 1.300.000,00</h2>
                    </div>
                    <div class="col-xs-12">
                        <div class="row">
                            <p class="imovel-descricao text-justify">Finamente mobiliado e decorado, na localização mais nobre e procurada do litoral! São 3 dormitórios sendo uma suíte, amplo living 3 ambientes, sacada de esquina com linda vista para o mar e Atlântida, churrasqueira na sacada, cozinha estilo americana, área de serviço separada, todas as peças muito bem iluminadas, split em todos os ambeintes, rebaixo em gesso, box de estacionamento. Aceita imóvel de menor valor, carro ou prazo</p>
                        </div>
                    </div>
                    <div class="col-xs-12 text-center">
                        <a href="#" class="btn btn-default imovel-url"><em>VEJA MAIS</em></a>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
<div class="container">
    <div class="col-md-offset-3 col-md-8">
        <div class="center-block" style="display: table;">
            <div id="pagination" class="pagination-holder clearfix">
                <div id="light-pagination" class="pagination light-theme simple-pagination">
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var filtro = <?= json_encode($filtro); ?>;
</script>
<script type="text/javascript" src="<?= base_url_filial('assets/js/filtro.js',false); ?>"></script>

<? $this->load->view('templates/menu-rodape'); ?>
<? $this->load->view('templates/footer'); ?>


