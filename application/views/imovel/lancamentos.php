<? $this->load->view('templates/header'); ?>
    <!--  LANCAMENTOS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/imovel/condominios-lancamentos.css'); ?>">
<? $this->load->view('templates/menu'); ?>
<? $this->load->view('templates/banner-logo-elemento', array('elemento' => '<img class="img-responsive" src="' . base_url('assets/images/banner-lancamentos.jpg') . '">')); ?>
<? $this->load->view('templates/filtro'); ?>

<div class="container container-conteudo">
    <h1>LANÇAMENTOS</h1>
    <? foreach ($cidades as $nome => $condominios) : ?>
        <div class="col-md-12 cidade-condominio-lancamento">
            <div class="col-md-3">
                <h4 class="text-center"><?= $nome; ?></h4>
            </div>
        </div>
        <div class="col-md-12">
            <? foreach ($lancamentos as $lancamento) : ?>
                <div class="col-md-3 text-center">
                    <div class="imovel">
                        <a href="<?= base_url('imovel?cod_imovel=' . $lancamento->f_codigo); ?>" title="Ir para <?= $lancamento->f_condominio; ?>">
                            <div class="imovel-img">
                                <img class="img-circle img-responsive" src="<?= $_SESSION['filial']['fotos_imoveis'] . $lancamento->foto . 'p.jpg'; ?>" onerror="this.src='<?= base_url('assets/images/imovel-sem-foto.jpg');?>'">
                            </div>
                            <div class="text-center">
                                <h3><?= $lancamento->f_condominio; ?></h3>
                            </div>
                        </a>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    <? endforeach; ?>
</div>

<? $this->load->view('templates/menu-rodape'); ?>
<? $this->load->view('templates/footer'); ?>