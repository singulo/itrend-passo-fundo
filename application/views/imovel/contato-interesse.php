<!-- Modal -->
<div class="modal fade" id="modal-contato-interesse" tabindex="-1" role="dialog" aria-labelledby="contatoInteresseModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Solicitar nosso contato</h4>
            </div>
            <div class="modal-body">
                <form id="form-interesse" onsubmit="cliente_enviar_contato_interesse(); return false;">
                    <div class="form-group col-xs-12">
                        <input type="hidden" value="<?= $imovel->id; ?>" name="cod_imovel">
                        <small>Você está solicitando o nosso contato para obter mais informações sobre o imóvel de cód. <?= $imovel->id; ?>.</small>
                        <textarea class="form-control" name="obs"  style="resize: vertical;" placeholder="Se desejar digite um mensagem para o nosso corretor não é obrigatório."></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="form-group col-xs-12">
                    <button type="button" onclick="cliente_enviar_contato_interesse();" data-loading-text="Aguarde..." class="btn btn-danger btn-enviar" autocomplete="off">Enviar</button>
                </div>
            </div>
        </div>
    </div>
</div>