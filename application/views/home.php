<? $this->load->view('templates/header'); ?>
<? $this->load->view('templates/menu'); ?>
<div class="hidden-xs hidden-sm">

<?// $this->load->view('templates/banner-principal'); ?>

<!--  PESQUISA -->
<link rel="stylesheet" type="text/css" href="<?= base_url_filial('assets/css/home.css'); ?>?version=1">


<? if ($_SESSION['filial']['chave'] == $this->config->item('filiais')['itrend-passo-fundo']['chave']): ?>
    <div class="col-xs-12">
        <div class="row">
            <div class="imagem-banner">
                <div class=" col-md-12 imagem-banner" style="background-image: url(<?= base_url_filial('assets/videos/circulos-logo-gradiente.png',false); ?>); background-repeat: no-repeat; background-position: center; position: absolute;z-index: 3; ">
                    <img class="center-block" src="<?= base_url_filial('assets/images/logo.png',false); ?>">
                </div>
                <!--            <div></div>-->
                <div class="col-md-12 imagem-banner" style="background-image: url(<?= base_url_filial('assets/images/banner/banner_passo_fundo.png',false)?>) ">

                </div>
            </div>
        </div>
    </div>
<? else :?>
    <div class="col-xs-12">
        <div class="row">
            <div class="video-banner">
                <div style="background-image: url(<?= base_url_filial('assets/videos/circulos-logo-gradiente.png',false); ?>); background-repeat: no-repeat; background-position: center; ">
                    <img class="center-block" src="<?= base_url_filial('assets/images/logo.png',false); ?>">
                </div>
    <!--            <div></div>-->
                <video muted autoplay loop>
                    <source src="<?= base_url_filial('assets/videos/capao_teaser.webm'); ?>" type="video/webm">
                </video>
            </div>
        </div>
    </div>
<?endif; ?>
</div>
<div class="visible-xs visible-sm">
    <? $this->load->view('templates/banner-logo-elemento', array('elemento' => '<img class="img-responsive" src="' . base_url_filial('assets/images/banner-capao-da-canoa.jpg',false) . '">')); ?>
</div>
<? $this->load->view('templates/filtro'); ?>
<? $this->load->view('templates/pesquisa-rapida'); ?>

<? if($_SESSION['filial']['chave'] == $this->config->item('filiais')['itrend-passo-fundo']['chave'] ) :?>
    <div class="col-xs-12" style="margin-bottom: 30px; height: 390px; background-image: url(<?= base_url_filial('assets/images/banner-passo-fundo.jpg',false); ?>); background-repeat: no-repeat; background-position: center; background-color: #732533;">

    </div>
<?else :?>
    <a href="<?= base_url_filial('capao-da-canoa'); ?>">
        <div class="col-xs-12" style="margin-bottom: 30px; background-image: url(<?= base_url_filial('assets/images/banner-capao.jpg',false); ?>); background-repeat: no-repeat; background-position: center; background-color: #732533;">
            <div class="container">
                <div class="col-md-5">
                    <h1 style="color: #fff; padding-left: 10%; padding-bottom: 30px; padding-top: 30px; font-size: 40px;"><em><b>CAPÃO<br>DA CANOA:</b></em><br>UMA DAS<br>CIDADES MAIS<br>TRADICIONAIS<br>DO LITORAL</h1>
                </div>
                <div class="col-md-6 hidden-xs hidden-sm">
                        <img style="margin-top: 35px;" class="img-responsive" src="<?= base_url_filial('assets/images/banner-passo-fundo.jpg',false); ?>">
                </div>
            </div>
        </div>
    </a>
<?endif;?>
<div class="container">
    <div class="col-xs-12">
        <div class="col-xs-12">
            <h3 class="text-center">Alguns dos nosso principais imóveis:</h3>
        </div>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs imoveis-complementos text-center" role="tablist">
            <? foreach ($imoveis as $complemento => $_imoveis) : ?>
                <button class="btn btn-danger btn-itrend text-uppercase" role="presentation" data-toggle="tab" data-target="#complemento-<?= md5($complemento); ?>"><?= $complemento; ?></button>
            <? endforeach; ?>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <? require_once(APPPATH . '../modules/simples/helpers/valor_imovel_formater_helper.php'); ?>
            <? foreach ($imoveis as $complemento => $_imoveis) : ?>
                <div role="tabpanel" class="tab-pane fade" id="complemento-<?= md5($complemento); ?>">
                    <? foreach ($_imoveis as $imovel) : ?>
                        <a href="<?= base_url_filial('imovel?id=' . $imovel->id); ?>">
                            <div class="imovel col-md-4 col-xs-12">
                                <div class="imovel-img"  style=" background-image: url(<?= $_SESSION['filial']['fotos_imoveis'] . $imovel->foto; ?>), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg',false);?>)">
                                </div>
                                <h3 class="no-margin-top titulo-imovel"><?= $_SESSION['filial']['tipos_imoveis'][$imovel->id_tipo]->tipo; ?></h3>
                                <p><?= $imovel->cidade; ?></p>
                                <p class="imovel-descricao"><?=  $imovel->descricao; ?></p>
                                <b><small>Valor do imóvel</small></b>
                                <h3 class="no-margin-top"><small><b>R$</b></small> <?= format_valor($imovel->valor); ?></h3>
                                <hr/>
                            </div>
                        </a>
                    <? endforeach; ?>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>


<? if($_SESSION['filial']['chave'] != $this->config->item('filiais')['itrend-passo-fundo']['chave'] ) :?>
    <div class="col-xs-12">
        <div class="row">
            <div id="banner-condominios" class="img-sombra">
                <div class="col-xs-3">
                    <a class="banner-previous pull-right" onclick="banner_condominios_previous_page();">
                        <span class="glyphicon glyphicon-menu-left"></span>
                    </a>
                </div>
                <div class="col-xs-6">
                    <div class="text-center hidden-xs hidden-sm">
                        <h2>veja os principais<br>condomínios do litoral</h2>
                    </div>
                    <ul>
                        <? foreach($condominios as $condominio) : ?>
                            <li>
                                <a href="<?= base_url_filial('condominio?id=' . $condominio->id); ?>">
                                    <div class="col-md-5">
                                        <img class="img-responsive" src="<?= $_SESSION['filial']['fotos_imoveis'] . $condominio->foto . '.jpg'; ?>" onerror="this.src='<?= base_url_filial('assets/images/imovel-sem-foto.jpg',false);?>'">
                                    </div>
                                    <div class="col-md-7 hidden-xs">
                                        <span><?= str_replace('cond.', '', strtolower($condominio->nome)); ?></span>
                                    </div>
                                </a>
                            </li>
                        <? endforeach; ?>
                    </ul>
                </div>
                <div class="col-xs-3">
                    <a class="banner-next" onclick="banner_condominios_next_page();">
                        <span class="glyphicon glyphicon-menu-right"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
<?endif; ?>

<div id="acao-banner" style="display: none;">
    <a href="<?= base_url_filial('regulamento'); ?>" title="Mais detalhes">
        <img height="550px" src="<?= base_url_filial('assets/images/acao-verao.jpg',false); ?>">
    </a>
</div>

<a id="banner-mulher" href="<?= base_url_filial('assets/images/dia_da_mulher.jpg',false); ?>"></a>

<? $this->load->view('templates/menu-rodape'); ?>
<? $this->load->view('templates/footer'); ?>

<script type="text/jscript" src="<?= base_url_filial('assets/js/home.js',false); ?>" ></script>

<script>
    $('.btn-itrend').on('click', function(){

        $('.btn-itrend').removeClass('btn-itrend-active');

        $($(this)[0]).addClass('btn-itrend-active');
    });

    //ATIVA PRIMEIRA TAB
    $('ul.nav.nav-tabs button[data-toggle="tab"]')[0].click();

    $(document).ready(function(){

       /* if( new Date('2017-03-10') < new Date() )
        {
            $.fancybox({
                'href' : '#acao-banner',
                //'iframe' : true,
                'closeClick'  : false
            });
        }

        if( new Date('2017-03-11') >= new Date() )
        {
            $('#banner-mulher').fancybox().click();
        }*/
    });
</script>