<? $this->load->view('templates/header'); ?>
<? $this->load->view('templates/menu'); ?>
<? $this->load->view('templates/banner-logo-elemento', array('elemento' => '<img class="img-responsive" src="' . base_url('assets/images/banner-quem-somos.jpg') . '">')); ?>
<? $this->load->view('templates/filtro'); ?>

<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/circle-pulse.css'); ?>">

<div class="container container-conteudo">
    <div class="row">
        <h1>SEJA BEM-VINDO A ITREND IMÓVEIS</h1>
        <div class="col-md-6 text-justify">
            <? if($_SESSION['filial']['chave'] == $this->config->item('filiais')['itrend-passo-fundo']['chave'] ) :?>
                <p>A iTrend Negócios Imobiliários Passo Fundo nasceu da necessidade de suprir lacunas que o mercado imobiliário de nossa amada cidade apresenta.</p> <p>Trazemos uma cultura de um mercado mais competitivo e voraz, como o do litoral norte, onde fomos agraciados recentemente com o PREMIO SINDUSCON 2016/2017 como a imobiliária mais lembrada e também com todo o know how da capital do nosso estado.</p><p>Estamos trazendo um novo modelo de intermediação competente aliada a agilidade e uma forma exclusiva de tratar com nossos clientes. </p> <p>Trabalhamos em um elevado nível e não estamos falando sobre somente os imóveis, que sim também são de alto padrão, para nós a excelência está na forma de atender nossos clientes, na infra estrutura que oferecemos e permanecendo sempre focados em oferecer a mais extraordinária experiência de compra do imóvel no padrão que você consquistou e a sua família merece, seja para seu lar ou para um investimento seguro para o seu futuro e de seus filhos.</p> <p>Por esse motivo dispomos de um seleto grupo de corretores associados  com experiência e expertise suficientes para oferecer serviços exclusivos, pois mais importante que clientes satisfeitos são clientes realizados.</p>
            <?else : ?>
                <p>A iTrend Negócios Imobiliários nasceu da paixão pela praia aliada com a vontade de criar uma cara nova para o mercado mais competitivo e voraz do litoral norte, misturando a inovação, sofisticação e profissionalismo em um só lugar. Trazendo a tendência intrínseca no DNA da organização, sabemos que ela é mutável e nós acompanhamos esse acontecimento, oferecendo sempre o melhor do mercado imobiliário. </p><p>Acreditamos que o mundo em que vivemos está cada dia mais conturbado e corrido, e queremos oferecer a você, amigo e cliente, oportunidades únicas de buscar o seu imóvel dos sonhos na praia. Temos a sua disposição uma equipe de profissionais de alta performance, especialistas em imóveis no litoral norte gaúcho focados em encontrar o lugar perfeito para você e sua família. Para nós, mais importante que vender imóveis feitos de tijolos e concretos é ajudar você a realizar seus sonhos, ter alegria, satisfação e bem-estar.</p><p>Levamos a sério a responsabilidade de construir lares, se depender de nós, o seu imóvel está aqui e nós vamos buscar ele juntos.</p>
            <?endif;?>
        </div>
        <div class="col-md-6">
            <iframe src="https://player.vimeo.com/video/172612332?autoplay=1&loop=1&color=ffffff&title=0&byline=0&portrait=0" style="width: 100%; min-height: 280px;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            <small>Filme Itrend - Capão da Canoa pelos olhos de quem ama.</small>
        </div>
    </div>
    <div class="row">
        <div class="col-md-offset-9 col-md-3">
            <div class="linha-horizontal"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-offset-1 col-md-4 text-right">
            <h3 class="cor-vermelho-itrend">Missão</h3>
            <p>Como missão queremos oferecer a você a excelência no atendimento buscando satisfazer todas suas expectativas. Nossos valores são a confiabilidade, profissionalismo, honestidade, transparência e acima de tudo respeito.</p>
        </div>
        <div class="col-md-2">
            <div style="margin-top: 80px; margin-left: 45%; position: relative; background: #45453f;">
                <div class="dot"></div>
                <div class="pulse"></div>
            </div>
        </div>
        <div class="col-md-4">
            <h3 class="cor-vermelho-itrend">Visão</h3>
            <p>Acreditamos que uma empresa de visão é aquela que acrescenta e faz a diferença no ramo em que escolheu atuar. Por isso a iTrend privilegia padrões de excelência e crescimento constante.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="linha-horizontal"></div>
        </div>
    </div>
</div>
<div class="bg-corretores">
    <?if($_SESSION['filial']['chave'] == $this->config->item('filiais')['itrend-passo-fundo']['chave']) :?>
            <div class="container container-conteudo" style="margin-bottom: 30px;padding-top: 40px;">
                <h1>Nosso diretor</h1>
                <div class="col-md-3">
                    <div class="corretor-quem-somos">
                        <img src="<?= base_url('assets/images/socios/alisson.jpg');?>" class="img-responsive">
                    </div>
                    <small class="text-right">Primeiramente, você precisa sonhar, saber onde pretende chegar e que tamanho quer ter.
                        Quando você faz o que gosta, fica mais motivado e envolvido no desempenho de qualquer atividade relacionada, mesmo que elas sejam obrigações. Isso significa mais produtividade em qualquer setor.
                        No longo desses meus 7 anos de mercado imobiliário três palavras definem nossos resultados extraordinários: Visão, Coragem e Competência.
                        Nessa crescente seguiremos trabalhando de forma clara para tornar nossos clientes realizados com seus lares e seus investimentos.</small>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="linha-horizontal"></div>
                        </div>
                    </div>
                    <p class="nome-corretor">Alisson Portella</p>
                </div>

            </div>
    <?else :?>
        <div class="container container-conteudo" style="margin-bottom: 30px;padding-top: 40px;">
            <h1>Nossos diretores</h1>
            <div class="col-md-3">
                <div class="corretor-quem-somos">
                    <img src="<?= base_url('assets/images/socios/evandro.jpg');?>" class="img-responsive">
                </div>
                <small class="text-right">Com 28 anos, se dedica a 12 anos ao mercado imobiliário. É o atual presidente da Associação dos Corretores de Imóveis de Capão da Canoa e vê na iTrend uma grande oportunidade, pois é uma empresa que entra no ramo com o objetivo de ser uma nova tendência. “Queremos mostrar que corretor e clientes podem e devem andar juntos.”, completa.</small>
                <br>
                <div class="row">
                    <div class="col-md-3">
                        <div class="linha-horizontal"></div>
                    </div>
                </div>
                <p class="nome-corretor">Evandro Ribeiro</p>
            </div>
            <div class="col-md-3">
                <div class="corretor-quem-somos">
                    <img src="<?= base_url('assets/images/socios/anderson.jpg');?>" class="img-responsive">
                </div>
                <small class="text-right">Com 27 anos e trabalha a três no mercado imobiliário, apaixonado pelo que faz acredita que está no caminho certo para ter uma carreira plena e sucesso profissional Tem como princípio básico de vida a honestidade, acredita que ter sucesso é olhar para trás e poder dizer “Eu consegui!”.</small>
                <br>
                <div class="row">
                    <div class="col-md-3">
                        <div class="linha-horizontal"></div>
                    </div>
                </div>
                <p class="nome-corretor">Anderson Ortiz Portella</p>
            </div>
            <div class="col-md-3">
                <div class="corretor-quem-somos">
                    <img src="<?= base_url('assets/images/socios/douglas.jpg');?>" class="img-responsive">
                </div>
                <small class="text-right">Atua há nove anos no mercado de imóveis, desde o início sempre teve como objetivo ser referência no mercado imobiliário, sonho este que cultivou e hoje coloca em prática sendo sócio fundador da ITREND Negócios Imobiliários, com espirito jovem determinado visando sempre o melhor negócio para seus clientes.</small>
                <br>
                <div class="row">
                    <div class="col-md-3">
                        <div class="linha-horizontal"></div>
                    </div>
                </div>
                <p class="nome-corretor">Douglas Silveira</p>
            </div>
            <div class="col-md-3">
                <div class="corretor-quem-somos">
                    <img src="<?= base_url('assets/images/socios/jeferson.jpg');?>" class="img-responsive">
                </div>
                <small class="text-right">Junto da ITREND deseja começar uma nova história. Com cinco anos de experiência na área, construiu-se como profissional com erros e acertos, que o tornaram a pessoa que é hoje. Nesse novo trajeto ao lado de sócios e profissionais extremamente capacitados aposta que essa jornada que tem tudo para dar certo. “Meu sonho é ver a ITREND em ascensão sempre, crescendo com os pés no chão”.</small>
                <br>
                <div class="row">
                    <div class="col-md-3">
                        <div class="linha-horizontal"></div>
                    </div>
                </div>
                <p class="nome-corretor">Jeferson Gimenez</p>
            </div>
            <br>
        </div>
    <?endif; ?>
</div>
<? if (count($corretores) > 0) :?>
    <div class="container container-conteudo">
        <div class="col-md-12">
            <h1 class="cor-vermelho-itrend">Corretores</h1>
            <? foreach($corretores as $corretor) : ?>
                <? if($corretor->nivel != ADMNivel::Secretaria && $corretor->nivel != ADMNivel::Agenciador) : ?>
                    <div class="col-md-3">
                        <img src="<?= $_SESSION['filial']['fotos_corretores'] . $corretor->id_corretor . '.jpg'; ?>" class="img-responsive" onError="this.src=$('#base_url').val() + 'assets/images/corretor-foto-padrao.png';" >
                        <p>
                            <?= $corretor->nome;?>
                            <br>
                            creci: <?= $corretor->creci;?>
                            <br>
                            <em><a href="mailto:<?= $corretor->email;?>" target="_top"><?= $corretor->email;?></a></em>
                        </p>
                    </div>
                <? endif;?>
            <? endforeach; ?>
        </div>
    </div>
<?endif;?>

<? $this->load->view('templates/menu-rodape'); ?>
<? $this->load->view('templates/footer'); ?>