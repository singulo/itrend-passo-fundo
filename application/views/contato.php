<? $this->load->view('templates/header'); ?>
<? $this->load->view('templates/menu'); ?>
<? $this->load->view('templates/banner-logo-elemento', array('elemento' => '<div id="map-canvas" data-lat-lng="' . $_SESSION['filial']['latitude_longitude'] . '" data-center="-29.767965, -50.025885" style="width: 100%; height: 500px;"></div>')); ?>
<? $this->load->view('templates/filtro'); ?>

<div class="container container-conteudo">
    <div class="col-xs-12">
        <h1>CONTATO</h1>

        <p>Olá, tudo bem?<br>A gente quer te ouvir! Envie suas dúvidas, sugestões e críticas para gente, vamos te responder assim que possível. É um prazer tê-lo em nosso site.</p>
    </div>

    <form id="form-contato" onsubmit="return false;">
        <div class="col-md-6">
            <? require_once(APPPATH . '../modules/simples/helpers/form_values_helper.php'); ?>
            <div class="form-group">
                <input class="form-control" type="text" name="nome" placeholder="NOME">
            </div>
            <div class="form-group">
                <input class="form-control" type="text" name="email" placeholder="EMAIL">
            </div>
            <div class="form-group">
                <input class="form-control telefone" type="text" name="telefone" placeholder="TELEFONE">
            </div>
            <div class="form-group">
                <input class="form-control" type="text" name="assunto" placeholder="ASSUNTO">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <textarea class="form-control" name="mensagem" placeholder="Sua mensagem para nós..."></textarea>
            </div>
        </div>
        <div class="col-xs-12">
            <button class="btn btn-default pull-right btn-enviar" data-loading-text="Aguarde..." autocomplete="off" onclick="cliente_enviar_novo_contato();">Enviar</button>
        </div>
    </form>
</div>

<? $this->load->view('templates/menu-rodape'); ?>
<? $this->load->view('templates/footer'); ?>

<style>
    #form-contato .form-control, .form-contato input[type=text]
    {
        background-color: #D4DDE1;
        border-radius: 0;
        color: #000 !important;
    }

    #form-contato textarea.form-control
    {
        height: 180px;
    }
</style>

<!--MAPA-->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<script src="<?= base_url('assets/js/gmap.js'); ?>"></script>