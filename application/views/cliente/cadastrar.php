<!-- Modal -->
<? ob_start(); ?>
<? require_once MODULESPATH . 'simples/helpers/endereco_helper.php'; ?>
<? ob_end_clean(); ?>
<?  $estados = obter_estados_brasil();?>
<div class="modal fade" id="modal-cadastrar" tabindex="-1" role="dialog" aria-labelledby="cadastrarModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Cadastre-se</h4>
            </div>
            <div class="modal-body">
                <form id="form-cadastrar">
                    <div class="form-group col-xs-12">
                        <input class="form-control" type="text" name="nome" placeholder="Nome">
                    </div>
                    <div class="form-group col-xs-12">
                        <input class="form-control" type="email" name="email" placeholder="Email">
                    </div>
                    <div class="form-group col-xs-12">
                        <input class="form-control telefone" type="text" name="telefone_1" placeholder="Telefone">
                    </div>
                    <div class="form-group col-xs-6">
                        <select name="uf" class="selectpicker" onchange="obter_cidades($('#form-cadastrar'));" data-live-search="true" title="Estado" data-width="100%">
                            <? foreach ($estados as $uf => $estado) : ?>
                                <option value="<?= strtoupper($uf) ?>"><?= $estado; ?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group col-xs-6">
                        <select name="cidade" class="selectpicker" data-live-search="true" disabled data-width="100%" title="Cidade"></select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="form-group col-xs-12">
                    <button type="button" onclick="cliente_cadastrar()" data-loading-text="Aguarde..." class="btn btn-danger btn-login" autocomplete="off">Cadastrar</button>
                </div>
            </div>
        </div>
    </div>
</div>
