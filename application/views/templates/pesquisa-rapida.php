<? $this->load->library('FiltroRapido'); ?>
<? $this->load->library('FiltroRapidoRegex'); ?>

<div class="col-xs-12 hidden-xs hidden-sm">
    <ul class="ch-grid">
        <li>
            <a href="<?= base_url('imovel/pesquisar?id_tipo=15&dormitorios=&id=&filtro-preco-min=&filtro-preco-max=')?>">
                <div class="ch-item" style="background-image: url(<?= base_url('assets/css/pesquisa-rapida/images/apartamentos.jpg'); ?>);">
                    <div class="ch-info">
                        <h3>APARTAMENTOS</h3>
                    </div>
                </div>
            </a>
        </li>
        <li>
            <a href="<?= base_url('imovel/pesquisar?id_tipo=3&dormitorios=&id=&filtro-preco-min=&filtro-preco-max=')?>">
                <div class="ch-item" style="background-image: url(<?= base_url('assets/css/pesquisa-rapida/images/condominio-fechado.jpg'); ?>);">
                    <div class="ch-info">
                        <h3>CASAS</h3>
                    </div>
                </div>
            </a>
        </li>

        <li>
            <a href="<?= base_url('imovel/pesquisar?id_tipo=5&dormitorios=&id=&filtro-preco-min=&filtro-preco-max=')?>">
                <div class="ch-item" style="background-image: url(<?= base_url('assets/css/pesquisa-rapida/images/terreno.jpg'); ?>);">
                    <div class="ch-info">
                        <h3>TERRENOS</h3>
                    </div>
                </div>
            </a>
        </li>
        <li>
            <a href="<?= base_url('imovel/pesquisar?id_tipo=16&dormitorios=&id=&filtro-preco-min=&filtro-preco-max=')?>">
                <div class="ch-item" style="background-image: url(<?= base_url('assets/css/pesquisa-rapida/images/casas-em-condominio.jpg'); ?>);">
                    <div class="ch-info">
                        <h3>CASAS EM CONDOMÍNIOS</h3>
                    </div>
                </div>
            </a>
        </li>
    </ul>
</div>

<div class="col-xs-12 btns-pesquisa-rapida hidden-md hidden-lg">
    <button class="btn btn-default btn-lg" onclick="pesquisa_rapida($('.form-filtro select.selectpicker[name=\'filtro-tipos[]\']'), '<?= implode(',', FiltroRapido::MontaFiltro(FiltroRapidoRegex::Casas)) . ',Casa Geminada'; ?>');">CASAS</button>
    <button class="btn btn-default btn-lg" onclick="pesquisa_rapida($('.form-filtro select.selectpicker[name=\'filtro-tipos[]\']'), '<?= implode(',', FiltroRapido::MontaFiltro(FiltroRapidoRegex::CasasEmCondominios)); ?>');">APARTAMENTOS</button>
    <button class="btn btn-default btn-lg" onclick="pesquisa_rapida($('.form-filtro select.selectpicker[name=\'filtro-tipos[]\']'), '<?= implode(',', FiltroRapido::MontaFiltro(FiltroRapidoRegex::CasasEmCondominios)); ?>');">CASAS EM CONDOMÍNIOS</button>
    <button class="btn btn-default btn-lg" onclick="pesquisa_rapida($('.form-filtro select.selectpicker[name=\'filtro-tipos[]\']'), 'Condomínio Fechado');">CONDOMÍNIOS FECHADOS</button>
</div>