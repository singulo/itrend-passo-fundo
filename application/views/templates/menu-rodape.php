<div class="col-xs-12 menu-rodape">
    <div class="container">
        <div class="col-md-4">
            <h3>
                <em class="text-uppercase"><?= $_SESSION['filial']['nome']; ?></em>
            </h3>
            <br>
            <br>
            Telefone: <?= $_SESSION['filial']['telefone_1']; ?>
            <br>
            Email: <?= $_SESSION['filial']['email_padrao']; ?>
            <br>
            <br>
            <?= $_SESSION['filial']['endereco']; ?>
            <br>
            Vergueiro - Passo Fundo - RS
            <br>
            <br>
            <h4><em>FAÇA UMA SOCIAL</em></h4>
            <a href="<?= $_SESSION['filial']['facebook']; ?>" target="_blank">
                <img class="img-responsive pull-left" src="<?= base_url('assets/images/facebook_icon.png'); ?>">
            </a>
            <a href="<?= $_SESSION['filial']['instagram']; ?>" target="_blank">
                <img class="img-responsive pull-left" src="<?= base_url('assets/images/instagram_icon.png'); ?>">
            </a>
            <br>
        </div>
        <div class="col-md-4 text-center">
            <h3><em>AQUI VOCÊ ENCONTRA</em></h3>
            <br>
            <br>
            <small>
                <? foreach($_SESSION['filial']['tipos_imoveis'] as $tipo) : ?>
                    <a href="<?= base_url('imovel/pesquisar?filtro-tipos=' . $tipo->id); ?>"><?= $tipo->tipo . ' | '; ?></a>
                <? endforeach; ?>
            </small>
        </div>
        <div class="col-md-4">
            <h3><em>FACEBOOK</em></h3>
            <br>
            <br>
            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fitrendpassofundo&tabs&width=340&height=130&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId" width="100%" height="130" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
        </div>
        <div class="col-md-12 text-center">
            <div class="row">
                <a href="http://www.singulo.com.br" target="_blank">
                    <img src="<?= base_url('assets/images/logo_singulo.png'); ?>">
                </a>
                <br>
                <small>&copy <?= date('Y'); ?> Todos os direitos reservados</small>
            </div>
        </div>
    </div>
</div>