    </body>

    <!--  MODERNIZR  -->
    <script type="text/jscript" src="<?= base_url_filial('assets/js/modernizr.js',false); ?>" ></script>

    <script type="text/jscript" src="<?= base_url_filial('assets/bootstrap-3.3.6/js/bootstrap.min.js',false); ?>" ></script>

    <!--  BANNER SLIDER  -->
    <script type="text/jscript" src="<?= base_url_filial('assets/js/flux.min.js',false); ?>" ></script>

    <!--   MULTISELECT  -->
    <script type="text/jscript" src="<?= base_url_filial('assets/bootstrap-select/bootstrap-select.min.js',false); ?>" ></script>

    <!--   VALUE SLIDER  -->
    <script type="text/jscript" src="<?= base_url_filial('assets/bootstrap-slider/bootstrap-slider.js',false); ?>" ></script>

    <!--  MENU  -->
    <script type="text/jscript" src="<?= base_url_filial('assets/js/menu.js',false); ?>" ></script>

    <!--  MASK  -->
    <script type="text/jscript" src="<?= base_url_filial('assets/admin/js/jquery.maskedinput.js',false); ?>"></script>
    <script type="text/jscript" src="<?= base_url_filial('assets/admin/js/jquery.maskMoney.js',false); ?>"></script>

    <!--  AUTONUMERIC  -->
    <script type="text/jscript" src="<?= base_url_filial('assets/admin/js/autoNumeric.min.js',false); ?>"></script>

    <!--  ALERTIFY  -->
    <script src="<?= base_url_filial('assets/admin/alertify/js/alertify.js',false); ?>"></script>

    <!--  VALIDATION  -->
    <script src="<?= base_url_filial('assets/admin/plugins/jquery-validation/jquery.validate.min.js',false); ?>"></script>

    <!--  JSONIFY  -->
    <script type="text/jscript" src="<?= base_url_filial('assets/simples/js/jsonify.js',false); ?>"></script>

    <!--  PAGINATION  -->
    <script type="text/javascript" src="<?= base_url_filial('assets/admin/js/jquery.simplePagination.js',false); ?>"></script>

    <!--FANCYBOX-->
    <script src="<?= base_url_filial('assets/fancybox/jquery.fancybox.pack.js',false); ?>"></script>
    <script src="<?= base_url_filial('assets/fancybox/jquery.mousewheel-3.0.6.pack.js',false); ?>"></script>

    <!--  IMOVEL  -->
    <script type="text/jscript" src="<?= base_url_filial('assets/simples/js/imovel.js',false); ?>"></script>

    <script src="<?=base_url_filial('modules/simples/assets/js/imovel.js',false)?>"></script>

    <script>
        var imoveis_tipos = <?= json_encode($_SESSION['filial']['tipos_imoveis']); ?>;
        var finalidades = <?= json_encode(array_flip(Finalidades::getConstants())); ?>;
        var condominios = <?= json_encode($_SESSION['filial']['condominios']); ?>;
    </script>

    <script type="text/jscript" src="<?= base_url_filial('assets/js/pesquisa.js',false); ?>"></script>
    <script src="<?=base_url_filial('assets/js/custom.js', false)?>"></script>
    <script src="<?=base_url_filial('modules/simples/assets/js/cliente.js', false)?>"></script>
    <script src="<?=base_url_filial('modules/simples/assets/js/contato.js', false)?>"></script>
    <script src="<?=base_url_filial('modules/simples/assets/js/simples.js',false)?>"></script>
<!--    <script src="--><?//=base_url_filial('modules/simples/assets/js/pesquisa.js',false)?><!--"></script>-->
    <script src="<?=base_url_filial('modules/simples/assets/js/gmap.js',false)?>"></script>



    <script>
        $(function(){

            if($('#slider').length > 0) {
                window.myFlux = new flux.slider('#slider', {
                    autoplay: true,
                    pagination: false,
                    controls: true,
                    transitions: ['concentric']
                });
            }
        });

        // JS start | Multiselect -->
        $('.selectpicker').selectpicker({
            style: 'btn-multselect',
            //size: 44
        });

        /* GOOGLER ANALYTICS */
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-87920979-1', 'auto');
        ga('send', 'pageview');
    </script>


<? if($_SESSION['filial']['chave'] == $this->config->item('filiais')['itrend-passo-fundo']['chave'] ) : ?>
    <!-- begin olark code -->
    <script type="text/javascript" async> ;(function(o,l,a,r,k,y){if(o.olark)return; r="script";y=l.createElement(r);r=l.getElementsByTagName(r)[0]; y.async=1;y.src="//"+a;r.parentNode.insertBefore(y,r); y=o.olark=function(){k.s.push(arguments);k.t.push(+new Date)}; y.extend=function(i,j){y("extend",i,j)}; y.identify=function(i){y("identify",k.i=i)}; y.configure=function(i,j){y("configure",i,j);k.c[i]=j}; k=y._={s:[],t:[+new Date],c:{},l:a}; })(window,document,"static.olark.com/jsclient/loader.js");
        /* custom configuration goes here (www.olark.com/documentation) */
        olark.identify('4778-882-10-6555');</script>
    <!-- end olark code -->
<? endif; ?>

</html>

