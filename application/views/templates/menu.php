<nav class="visible-md visible-lg navbar navbar-default navbar-fixed-top">
    <div class="container">
        <ul class="nav navbar-nav">
            <li><a href="<?= base_url(); ?>" style="padding-top: 12px; padding-bottom: 13px; background-color: transparent !important;"><img style="height: 25px;" src="<?= base_url('assets/images/logo-branco.png'); ?>"></a></li>
            <li><a href="<?= base_url_filial('quem-somos'); ?>">QUEM SOMOS</a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">IMÓVEIS <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="<?= base_url('imovel/pesquisar'); ?>">PESQUISAR</a></li>
                    <? if ($_SESSION['filial']['chave'] != $this->config->item('filiais')['itrend-passo-fundo']['chave']): ?>
                        <li><a href="<?= base_url_filial('condominio/lancamentos'); ?>">LANÇAMENTOS</a></li>
                        <li><a href="<?= base_url_filial('condominio/lista'); ?>">CONDOMÍNIOS</a></li>
                    <?endif; ?>
                </ul>
            </li>
            <? if($_SESSION['filial']['chave'] != $this->config->item('filiais')['itrend-passo-fundo']['chave'] ) :?>
                <li><a href="<?= base_url_filial('capao-da-canoa'); ?>">CAPÃO DA CANOA</a></li>
            <?endif; ?>
            <li><a href="<?= base_url_filial('contato'); ?>">CONTATO</a></li>
            <li><a href="<?= base_url_filial('historico'); ?>" style="<? if(!$this->session->has_userdata('usuario')) echo 'display:none'; ?>">HISTÓRICO</a></li>
        </ul>
        <div class="pull-right">
            <ul class="nav navbar-nav">
                <? if($this->session->has_userdata('usuario')) : ?>
                    <li><a href="#modal-dados" data-toggle="modal"><?= $this->session->userdata('usuario')->nome; ?></a>
                <? else : ?>
                    <li><a href="#modal-login" data-toggle="modal">Entrar/Cadastrar</a>
                <? endif; ?>
            </ul>
        </div>
    </div>
    <div class="menu-pesquisa-rapida">
        <ul>
            <li>
                <a href="<?= base_url('imovel/pesquisar?id_tipo=15&dormitorios=&id=&filtro-preco-min=&filtro-preco-max=')?>">
                APARTAMENTOS
                </a>
            </li>
            <li>
                <a href="<?= base_url('imovel/pesquisar?id_tipo=3&dormitorios=&id=&filtro-preco-min=&filtro-preco-max=')?>">
                    CASAS
                </a>
            </li>
            <li>
                <a href="<?= base_url('imovel/pesquisar?id_tipo=5&dormitorios=&id=&filtro-preco-min=&filtro-preco-max=')?>">
                    TERRENOS
                </a>
            </li>
        </ul>
    </div>
</nav>

<nav role="navigation" class="visible-xs visible-sm navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="<?= base_url(); ?>" class="navbar-brand"><img style="height: 20px;" src="<?= base_url('assets/images/logo-branco.png'); ?>"></a>
        </div>
        <!-- Collection of nav links and other content for toggling -->
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="<?= base_url('quem-somos'); ?>">QUEM SOMOS</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">IMÓVEIS <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?= base_url('imovel/pesquisar'); ?>">PESQUISAR</a></li>
                        <li><a href="<?= base_url('imovel/lancamentos'); ?>">LANÇAMENTOS</a></li>
                        <li><a href="<?= base_url('imovel/condominios'); ?>">CONDOMÍNIOS</a></li>
                    </ul>
                </li>
                <li><a href="<?= base_url('capao-da-canoa'); ?>">CAPÃO DA CANOA</a></li>
                <li><a href="<?= base_url('contato'); ?>">CONTATO</a></li>
                <li><a href="<?= base_url('historico'); ?>" style="<? if(!$this->session->has_userdata('usuario')) echo 'display:none'; ?>">HISTÓRICO</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <? if($this->session->has_userdata('usuario')) : ?>
                    <li><a href="#modal-dados" data-toggle="modal"><?= $this->session->userdata('usuario')->nome; ?></a>
                <? else : ?>
                    <li><a href="#modal-login" data-toggle="modal">Entrar/Cadastrar</a>
                <? endif; ?>
            </ul>
        </div>
    </div>
</nav>

<? $this->load->view('cliente/login'); ?>
<? $this->load->view('cliente/cadastrar'); ?>
<? $this->load->view('imovel/contato-interesse'); ?>

<? if($this->session->has_userdata('usuario')) : ?>
    <? $this->load->view('cliente/dados'); ?>
<?endif; ?>

