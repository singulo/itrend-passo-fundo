<? require_once MODULESPATH . 'simples/helpers/simples_helper.php'; ?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title><?= $_SESSION['filial']['nome']; ?></title>
    <link rel="icon" href="<?= base_url('assets/images/favicon.ico'); ?>" type="image/x-icon">

    <!-- CSS Bootstrap | Arquivos padrões -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/bootstrap-3.3.6/css/bootstrap.min.css'); ?>">

    <!--  BOOTSTRAP MULTSELECT -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/bootstrap-select/bootstrap-select.min.css'); ?>">

    <!--  BOOTSTRAP VALOR SLIDER  -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/bootstrap-slider/bootstrap-slider.css'); ?>">

    <!--  ALERTIFY  -->
    <link href="<?= base_url('assets/admin/alertify/css/alertify.css'); ?>" rel="stylesheet" type="text/css"/>

    <!--  PAGINATION  -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/admin/css/simplePagination.css'); ?>">

    <!--  PESQUISA RAPIDA  -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/pesquisa-rapida/pesquisa-rapida-effects.css'); ?>">

    <!-- FANCYBOX -->
    <link rel="stylesheet" href="<?= base_url('assets/fancybox/jquery.fancybox.css'); ?>">

    <!--  CUSTOM  -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/custom.css'); ?>">

    <!--  Font Google  -->
    <link href='https://fonts.googleapis.com/css?family=Istok+Web:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link rel="apple-touch-icon" href="<?= base_url('assets/images/icones-mobile/Icon-Small-50@2x.png'); ?>">

    <!--    JS -->
    <script type="text/jscript" src="<?= base_url('assets/js/jquery-2.2.0.min.js'); ?>" ></script>

    <? if(isset($appendTags)) : ?>
        <? foreach($appendTags as $tag) : ?>
            <?= $tag; ?>
        <? endforeach; ?>
    <? endif; ?>

</head>
<body id="page-wrap">

    <input id="base_url" type="hidden" value="<?= base_url()?>">
    <input id="filial_fotos_imoveis" type="hidden" value="<?= $_SESSION['filial']['fotos_imoveis']; ?>">
    <input id="filial_fotos_corretores" type="hidden" value="<?= $_SESSION['filial']['fotos_corretores'];?>">
    <input id="filial_sessao" type="hidden" value="<? if(isset($_SESSION['filial']['chave'])) {echo  $_SESSION['filial']['chave'];}?>">
    <input id="usuario" type="hidden" value='<?= json_encode($this->session->userdata('usuario')); ?>'>

