<div class="container visible-md visible-lg">
    <form class="col-md-10 form-filtro form-inline" id="filtro-imovel" action="<?= base_url('imovel/pesquisar'); ?>">
        <div class="form-group col-md-2">
            <select class="selectpicker form-control" name="finalidade" multiple title="Finalidade">
                <? foreach( Finalidades::getConstants() as $finalidade => $valor ) : ?>
                    <? if($finalidade != 'Temporada') :?>
                        <option value="<?= $valor;?>"><?= $finalidade; ?></option>
                    <?endif; ?>
                <? endforeach; ?>
            </select>
        </div>

        <div class="form-group col-md-2">
            <select class="selectpicker form-control" name="id_tipo" data-live-search="true" title="Tipo" multiple data-selected-text-format="count > 1" data-count-selected-text="{0} tipos">
                <? foreach($_SESSION['filial']['tipos_imoveis'] as $tipo) : ?>
                    <option value="<?= $tipo->id; ?>"><?= $tipo->tipo; ?></option>
                <? endforeach; ?>
            </select>
        </div>
        <div class="form-group col-md-2">
            <select class="selectpicker form-control" name="dormitorios"  title="Dorm">
                <option value="">-</option>
                <? for ($i = 1; $i <= $this->config->item('pesquisa')['dormitorios']; $i++) : ?>
                <option value="<?= $i; ?>"><?= $i; ?></option>
                <? endfor; ?>
            </select>
        </div>
        <div class="form-group col-md-1">
            <input type="text" name="id" class="form-control" placeholder="CÓD." style="width: 80px;">
        </div>
        <!--<div class="form-group col-md-2">
            <input type="hidden" name="filtro-preco-min">
            <input type="hidden" name="filtro-preco-max">
            <input type="text" class="slider-filtro span2" value="" data-slider-handle="custom" data-slider-min="0" data-slider-max="3000000" data-slider-step="50000" data-slider-value="[0,3000000]" data-slider-ticks-labels='["R$0", "R$3.000.000"]'/>
        </div>-->

        <div class="form-group col-md-2">
            <input type="text" name="preco_min" class="form-control dinheiro" placeholder="Valor mínimo">
        </div>
        <div class="form-group col-md-2">
            <input type="text" name="preco_max" class="form-control dinheiro" placeholder="Valor máximo">
        </div>

        <div class="form-group col-md-1">
            <button type="button" class="btn btn-default pull-right" onclick="pesquisar(0); " style="width: 100%; padding: 8px 1px; font-size: 12px;">PESQUISAR</button>
        </div>
    </form>
</div>
<script>
    var filtro = <?= json_encode($filtro); ?>;
</script>

