<? $this->load->view('templates/header'); ?>
<? $this->load->view('templates/menu'); ?>
<? $this->load->view('templates/banner-logo-elemento', array('elemento' => '<img class="img-responsive" src="' . base_url('assets/images/banner-capao-da-canoa.jpg') . '">')); ?>
<? $this->load->view('templates/filtro'); ?>

<div class="container container-conteudo">
    <div class="col-xs-12">
        <h1>CAPÃO DA CANOA</h1>
    </div>
    <div class="col-xs-12 embed-container">
        <iframe src="https://player.vimeo.com/video/172612332?autoplay=1&loop=1&color=ffffff&title=0&byline=0&portrait=0" style="width: 100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    </div>
    <div class="col-md-7" style="border-right:solid 1px #b59797">
        <p>Nem sempre a Capão da Canoa teve esse nome, Arroio da Pescaria, como era chamada em 1900 além de pescadores abrigava alguns aventureiros. Seus primeiros ranchos foram formados a beira mar de maneira precária e a comida era provida da pescaria. Em 1920 começaram a chegar os primeiros veranistas vindos da serra gaúcha e também de Porto Alegre, para pequenos hotéis iluminados a luz de velas. Em 1982 emancipou-se então a nossa querida Capão da Canoa.</p>
        <p>Como não ser apaixonado por este lugar? É onde tudo acontece no litoral gaúcho, uma praia que também é cidade, frequentada por todas as faixas etárias. São 19,1 km de extensão de uma praia que já foi marcada por muitos acontecimentos, berço do concurso Garota Verão, o inesquecível Bar Onda, que hoje é um local onde diversos artistas se fazem shows, as apresentações da Banda da Brigada Militar, a linda estátua de Iemanjá, o calçadão onde todos se encontram.</p>
        <p>A cidade litorânea hoje conta com 42.000 habitantes, um lugar completo com pouco mais de 30 anos e uma infraestrutura ótima para quem quer aproveitar a família e ter qualidade de vida.</p>
    </div>
    <div class="col-md-5"><br>
        <h1>Capão da Canoa pelos olhos de quem ama!</h1>
    </div>
</div>
<? $this->load->view('templates/menu-rodape'); ?>
<? $this->load->view('templates/footer'); ?>

<style>
    .embed-container
    {
        margin-bottom: 30px;
    }

    .embed-container iframe
    {
        height: 450px;
        width: 100%;
    }
</style>