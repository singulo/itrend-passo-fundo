<?php

class FiltroRapido
{
    public static function MontaFiltro($regex)
    {
        $tipos = array();
        foreach($_SESSION['filial']['tipos_imoveis'] as $tipo)
            $tipos[] = $tipo->tipo;

        $tipos = preg_grep($regex, $tipos);

        $filtro = array();
        foreach($tipos as $tipo)
            $filtro[$tipo] = $tipo;

        return $filtro;
    }
}