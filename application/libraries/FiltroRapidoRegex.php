<?php

class FiltroRapidoRegex
{
    const Casas                 = '/Casa/';
    const Apartamentos          = '/Apartamento/';
    const CasasEmCondominios    = '/Casa em [Cc]ondomínio/';
}