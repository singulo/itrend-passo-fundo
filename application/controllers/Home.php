<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH."../modules/simples/core/Base_Controller.php";

/**
 * @property Imovel_Model $imovel_model
 */
class Home extends Base_Controller
{
	public function index()
	{
		$this->load->model('simples/imovel_model');

		$data['imoveis']['destaque venda'] = $this->imovel_model->destaques_por_status(12, true,3);
		$data['imoveis']['destaque locacao'] = $this->imovel_model->destaques_por_status(12, true,2);
//        $data['imoveis']['imovel na planta'] = $this->imovel_model->destaques_por_status(12, true,1);



        $this->load->view('home', $data);
	}
}
