<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH."../modules/simples/core/Base_Controller.php";

/**
 * @property Clientes_imoveis_log_model $clientes_imoveis_log_model
 */
class Historico extends Base_Controller
{
	public function index()
	{
		$this->load->model('simples/clientes_imoveis_log_model');

		$imoveis = $this->clientes_imoveis_log_model->imoveis_visualizados($this->session->userdata('usuario')->email);

		$data['dias'] = array();

		foreach($imoveis as $imovel)
		{
			$data['dias'][$imovel->dia_visualizacao][] = $imovel;
		}

		$this->load->view('historico', $data);
	}
}
