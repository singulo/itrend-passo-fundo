<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH."../modules/simples/controllers/Base_imovel_controller.php";

class Imovel extends Base_Imovel_Controller
{
	public function index()
	{
		$data = parent::index();

//		var_dump($data['imovel']->condominio);
//		die();
		$this->load->view('imovel/detalhe', $data);
	}


	public function condominios()
	{
		$data = parent::condominios();

		foreach($data['condominios'] as $condominio)
		{
			$data['cidades'][$condominio->f_cidade][] = $condominio;
		}

		$this->load->view('imovel/condominios', $data);
	}

	public function lancamentos()
	{
		$data = parent::lancamentos();

		foreach($data['lancamentos'] as $lancamento)
		{
			$data['cidades'][$lancamento->f_cidade][] = $lancamento;
		}

		$this->load->view('imovel/lancamentos', $data);
	}

	public function pesquisar()
	{
//		var_dump($_SESSION['filial']['tipos_imoveis']);
//		die();

		$data = parent::pesquisar();

		if(is_null($data['filtro']->preco_min) || $data['filtro']->preco_min == '')
			$data['filtro']->preco_min = 0;

		if(is_null($data['filtro']->preco_max) || $data['filtro']->preco_max == '')
			$data['filtro']->preco_max = 3000000;
		
		$data['filtro']->preco_min = $this->transforma_valor_imovel($data['filtro']->preco_min);
		$data['filtro']->preco_max = $this->transforma_valor_imovel($data['filtro']->preco_max);

		$this->load->view('imovel/pesquisa', $data);
	}

	public function buscar()
	{
		$data = parent::buscar();
		$data['status'] = true;

		echo json_encode($data);
	}
}
